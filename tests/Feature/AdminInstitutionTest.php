<?php

namespace Tests\Feature;

use App\Models\Institution;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AdminInstitutionTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function guest_cannot_see_institution_listing()
    {
        $response = $this->get(route('institutions.index'));

        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }

    /**
     * @test
     */
    public function admin_show_list_institutions()
    {
        $this->createAdminAndAuth();

        $response = $this->get(route('institutions.index'));

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function admin_view_create_page_institutions()
    {
        $this->createAdminAndAuth();

        $response = $this->get(route('institutions.create'));

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function admin_create_institution_success()
    {
        $this->createAdminAndAuth();

        $data = $this->getDataSuccess();

        $response = $this->post(route('institutions.store'), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('institutions.index'));
        $this->assertDatabaseHas('institutions', $data);
    }

    /**
     * @test
     */
    public function admin_create_institution_error()
    {
        $this->createAdminAndAuth();

        $data = $this->getDataError();

        $response = $this->json('post', route('institutions.store'), $data);
        $response->assertStatus(422);
    }

    /**
     * @test
     */
    public function admin_view_edit_page_institutions()
    {
        $this->createAdminAndAuth();

        $institution = Institution::factory()->create();

        $response = $this->get(route('institutions.edit', $institution->id));

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function admin_edit_institution_success()
    {
        $this->createAdminAndAuth();

        $institution = Institution::factory()->create();

        $data =  $this->getDataSuccess();

        $response = $this->json('put', route('institutions.update', $institution->id), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('institutions.show', $institution->id));
        $this->assertDatabaseHas('institutions', $data);
    }

    /**
     * @test
     */
    public function admin_edit_institution_error()
    {
        $this->createAdminAndAuth();

        $institution = Institution::factory()->create();

        $data = $this->getDataError();

        $response = $this->json('put', route('institutions.update', $institution->id), $data);
        $response->assertStatus(422);
    }

    private function createAdminAndAuth()
    {
        $user = User::factory()->create();
        $user->assignRole('admin');

        $this->actingAs($user);
    }

    private function getDataSuccess()
    {
        return [
            'name' => 'Minha instituição',
            'cnpj' => '96325874123654',
            'description' => 'Descrição da instituição',
        ];
    }

    private function getDataError()
    {
        return [
            'name' => 'int',
            'cnpj' => '234',
            'description' => 'Desc',
        ];
    }
}
