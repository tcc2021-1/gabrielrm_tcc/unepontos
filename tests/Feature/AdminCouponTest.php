<?php

namespace Tests\Feature;

use App\Models\Company;
use App\Models\Coupon;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AdminCouponTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function guest_cannot_see_coupon_listing()
    {
        $response = $this->get(route('coupons.index'));

        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }

    /**
     * @test
     */
    public function admin_show_list_coupons()
    {
        $this->createAdminAndAuth();

        $response = $this->get(route('coupons.index'));

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function admin_view_create_page_coupons()
    {
        $this->createAdminAndAuth();

        $response = $this->get(route('coupons.create'));

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function admin_create_coupon_success()
    {
        $this->createAdminAndAuth();

        $data = $this->getDataSuccess();

        $response = $this->post(route('coupons.store'), $data);
        $coupon = Coupon::first();

        $response->assertStatus(302);
        $response->assertRedirect(route('coupons.show', [$coupon->id]));
        $this->assertDatabaseHas('coupons', $data);
    }

    /**
     * @test
     */
    public function admin_create_coupon_error()
    {
        $this->createAdminAndAuth();

        $data = $this->getDataError();

        $response = $this->json('post', route('coupons.store'), $data);
        $response->assertStatus(422);
    }

    /**
     * @test
     */
    public function admin_view_edit_page_coupons()
    {
        $this->createAdminAndAuth();

        $coupon = Coupon::factory()->create();

        $response = $this->get(route('coupons.edit', $coupon->id));

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function admin_edit_coupon_success()
    {
        $this->createAdminAndAuth();

        $coupon = Coupon::factory()->create();

        $data =  $this->getDataSuccess();

        $response = $this->json('put', route('coupons.update', $coupon->id), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('coupons.index'));
        $this->assertDatabaseHas('coupons', $data);
    }

    /**
     * @test
     */
    public function admin_edit_coupon_error()
    {
        $this->createAdminAndAuth();

        $coupon = Coupon::factory()->create();

        $data = $this->getDataError();

        $response = $this->json('put', route('coupons.update', $coupon->id), $data);
        $response->assertStatus(422);
    }

    /**
     * @test
     */
    public function admin_edit_coupon_rescued()
    {
        $this->createAdminAndAuth();

        $coupon = Coupon::factory()->rescued()->create();

        $data = $this->getDataSuccess();

        $response = $this->json('put', route('coupons.update', $coupon->id), $data);

        $response->assertStatus(302);
    }

    private function createAdminAndAuth()
    {
        $user = User::factory()->create();
        $user->assignRole('admin');

        $this->actingAs($user);
    }

    private function getDataSuccess()
    {
        return [
                'company_id' => Company::factory()->create()->id,
                'points' => 350,
            ];
    }

    private function getDataError()
    {
        return [
            'company_id' => 'as',
            'points' => 350000,
        ];
    }
}
