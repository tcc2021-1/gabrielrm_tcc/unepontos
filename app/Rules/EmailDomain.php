<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class EmailDomain implements Rule
{
    const VALID_EMAIL_DOMAINS = [
        'gmail.com',
        'hotmail.com',
        'outlook.com',
        'live.com',
        'yahoo.com',
        'hotmail.com.br',
        'outlook.com.br',
        'live.com.br',
        'yahoo.com.br',
        'uol.com.br',
        'bol.com.br',
        'msn.com.br',
    ];

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function passes($attribute, $value)
    {
        list($username, $domain) = explode('@', $value . "@");
        if (empty($domain)) {
            return false;
        }

        return in_array($domain, self::VALID_EMAIL_DOMAINS);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.custom.email.email_domain');
    }
}
