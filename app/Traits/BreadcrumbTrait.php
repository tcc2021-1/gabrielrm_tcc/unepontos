<?php

namespace App\Traits;

trait BreadcrumbTrait
{
    static function getBreadcrumbLevelOne(string $currentPage): array
    {
        return [
            [
                'title' => 'Inicio',
                'url' => route('home')
            ],
            [
                'title' => $currentPage,
                'url' => null
            ],
        ];
    }

    static function getBreadcrumbLevelTwo(string $title, string $route, string $currentPage): array
    {
        return [
            [
                'title' => 'Inicio',
                'url' => route('home')
            ],
            [
                'title' => $title,
                'url' => $route
            ],
            [
                'title' => $currentPage,
                'url' => null
            ],
        ];
    }
}
