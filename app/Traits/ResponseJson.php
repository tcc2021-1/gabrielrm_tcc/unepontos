<?php

namespace App\Traits;

trait ResponseJson
{

    static function responseJson(bool $success, array $data, int $status = 200): \Illuminate\Http\JsonResponse
    {
        return response()->json([
            'success' => $success,
            'data' => $data
        ], $status);
    }
}
