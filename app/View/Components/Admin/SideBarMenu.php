<?php

namespace App\View\Components\Admin;

use App\Models\Menu;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\Component;

class SideBarMenu extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.admin.side-bar-menu');
    }

    /**
     * @return mixed
     */
    public function menus()
    {
        $userId = Auth::id();
        $menu = Menu::select('menus.*')
            ->join('menu_roles','menus.id','menu_roles.menu_id')
            ->join('roles','roles.id','menu_roles.role_id')
            ->join('model_has_roles','roles.id','model_has_roles.role_id')
            ->where('parent_id', '=', 0)
            ->where('model_id','=', $userId)
            ->orderBy('order','asc')
            ->get();

        foreach($menu as $key => $value){
            $children = Menu::join('menu_roles','menus.id','menu_roles.menu_id')
                ->join('roles','roles.id','menu_roles.role_id')
                ->join('model_has_roles','roles.id','model_has_roles.role_id')
                ->where('parent_id', '=', $value->id)
                ->where('model_id','=', $userId)
                ->orderBy('order','asc')
                ->get();

            $menu[$key]->children = $children;
        }
        return $menu;
    }
}
