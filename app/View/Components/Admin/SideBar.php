<?php

namespace App\View\Components\Admin;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\Component;

class SideBar extends Component
{
    protected $user;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->user = User::whereId(Auth::id())->with('media')->first();
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.admin.side-bar');
    }

    /**
     * @return string
     */
    public function userProfileImage(): string
    {
        $userProfileImage = asset('/images/default-user.png');
        if (count($this->user['media'])) {
            $userProfileImage = $this->user->getLastImageUrl('profile');
        }

        return $userProfileImage;
    }

    /**
     * @return string
     */
    public function userName(): string
    {
        return $this->user->name;
    }
}
