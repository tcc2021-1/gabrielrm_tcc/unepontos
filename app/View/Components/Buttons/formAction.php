<?php

namespace App\View\Components\Buttons;

use Illuminate\View\Component;

class formAction extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(public string $routeBack,
                                public string $descriptionAction,
                                public string $iconAction,
                                public ?string $routeAction = null,
                                public bool $submit = false
    )
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.buttons.form-action');
    }
}
