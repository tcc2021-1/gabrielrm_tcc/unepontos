<?php

namespace App\View\Components\Show;

use Illuminate\View\Component;

class FormGroup extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(public string $description)
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.show.form-group');
    }
}
