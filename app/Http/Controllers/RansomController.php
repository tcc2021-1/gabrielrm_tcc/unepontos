<?php

namespace App\Http\Controllers;

use App\Http\Requests\Admin\DonationRequest;
use App\Http\Requests\Admin\RansomRequest;
use App\Http\Requests\Admin\RansomUpdateRequest;
use App\Models\Donation;
use App\Repositories\Contracts\DonationRepositoryInterface;
use App\Repositories\Contracts\InstitutionRepositoryInterface;
use App\Repositories\Contracts\WalletRepositoryInterface;
use App\Traits\BreadcrumbTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class RansomController extends Controller
{
    use BreadcrumbTrait;
    private DonationRepositoryInterface $donationRepository;
    private InstitutionRepositoryInterface $institutionRepository;
    private WalletRepositoryInterface $walletRepository;



    private string $route = "rescues.";
    private string $viewDir = 'ransom.';
    private string $titlePage = 'Resgates';

    public array $page = [];

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct(
        DonationRepositoryInterface $donationRepository,
        InstitutionRepositoryInterface $institutionRepository,
        WalletRepositoryInterface $walletRepository,
    )
    {

        $this->donationRepository = $donationRepository;
        $this->institutionRepository = $institutionRepository;
        $this->walletRepository = $walletRepository;

        $this->page = [
            'titlePage' => $this->titlePage,
            'route' => $this->route,
            'viewDir' => $this->viewDir,
            'breadcrumb' => [
                [
                    'title' => 'Inicio',
                    'url' => route('home')
                ],
                [
                    'title' => $this->titlePage,
                    'url' => null
                ],
            ]
        ];

        $this->middleware('permission:rescue-list');
        $this->middleware('permission:rescue-create', ['only' => ['create','store']]);
    }

    /**
     * Show the application admin.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        if (! $user->can('rescue-list')) {
            toastr()->error('Usuário sem permissão');
            return redirect()->back();
        }

        $data = [];

        if ($user->hasRole('admin')) {
            $data = $this->donationRepository->getAllRansomRequests();
        }

        if ($user->hasRole('institution') && isset($user->institution->id)) {
            $data = $this->donationRepository->getRansomRequestsByInstitutionId($user->institution->id);
        }

        return view($this->viewDir.'index')->with(['page' => $this->page, 'data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        $user = Auth::user();
        if (! $user->can('rescue-create')) {
            toastr()->error('Usuário sem permissão');
            return redirect()->back();
        }

        $data = [];

        if ($user->hasRole('admin')) {
            $data['institutions'] = $this->institutionRepository->getInstitutionsPluckIdAndName();
        }

        if (! $user->hasRole('admin') && ! isset($user->institution->id)) {
            toastr()->warning('Usuário não vinculado a uma instituição');
            return redirect()->back();
        }

        if ($user->hasRole('institution') && isset($user->institution->id)) {
            $data['institutions'] = $this->institutionRepository->getByAttribute('user_id', $user->id)->with('user')->get()->pluck('name', 'id');
        }

        $this->page['breadcrumb'] = self::getBreadcrumbLevelTwo($this->page['titlePage'], route($this->page['route'].'index'), 'Criar');
        return view($this->viewDir.'create')->with([
            'page'=>$this->page, 'data' => $data
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RansomRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RansomRequest $request)
    {
        $user = Auth::user();
        if (! $user->can('rescue-create')) {
            toastr()->error('Usuário sem permissão');
            return redirect()->back();
        }

        $data = $request->validated();

        if (! $user->hasRole('institution') && ! $user->hasRole('admin')) {
            toastr()->warning('Usuário não vinculado aos papéis');
            return redirect()->back();
        }

        if (! $user->hasRole('admin') && isset($user->institution->id) && $user->institution->id != $data['institution_id']) {
            toastr()->warning('Usuário não pode solicitar resgates');
            return redirect()->back();
        }

        $availableBalance = $this->donationRepository->sumAvailablePoints($data['institution_id']);

        if (empty($availableBalance) || config('une.min_points_request_rescue') > $availableBalance) {
            toastr()->error('Saldo indisponível para concluir operação');
            return redirect()->back();
        }

        if ($this->donationRepository->createRequestRansom($user->id, $data['institution_id'])) {
            toastr()->success('Solicitação de resgate realizada com sucesso');
            return redirect()->route($this->route.'index');
        }

        toastr()->error('Tivemos desafios para concluir a operação, contate o suporte');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $user = Auth::user();
        if (! $user->can('rescue-list')) {
            toastr()->error('Usuário sem permissão');
            return redirect()->back();
        }

        $data = $this->donationRepository->findWithRelationsById($id);

        if (empty($data)) {
            toastr()->warning('Solicitação não encontrada');
            return redirect()->back();
        }

        $this->page['breadcrumb'] = self::getBreadcrumbLevelTwo($this->page['titlePage'], route($this->page['route'].'index'), 'Visualizar');
        return view($this->viewDir.'show')
            ->with([
                'page'=>$this->page,
                'data'=>$data
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $user = Auth::user();
        if (! $user->can('rescue-edit')) {
            toastr()->error('Usuário sem permissão');
            return redirect()->back();
        }

        $data = $this->donationRepository->findWithRelationsById($id);

        if (empty($data)) {
            toastr()->warning('Solicitação não encontrada');
            return redirect()->back();
        }

        $data['option_status'] = [
            'rescued' => __('status.rescued'),
            'pending' => __('status.pending'),
            'suspended' => __('status.suspended'),
            'cancelled' => __('status.cancelled')
        ];

        $this->page['breadcrumb'] = self::getBreadcrumbLevelTwo($this->page['titlePage'], route($this->page['route'].'index'), 'Editar');
        return view($this->viewDir.'edit')
            ->with([
                'page'=>$this->page,
                'data'=>$data
            ]);
    }

    public function update(RansomUpdateRequest $request, int $ransomId)
    {
        $user = Auth::user();
        if (! $user->can('rescue-edit')) {
            toastr()->error('Usuário sem permissão');
            return redirect()->back();
        }

        $ransom = $this->donationRepository->find($ransomId);
        if (empty($ransom)) {
            toastr()->warning('Solicitação não encontrada');
            return redirect()->back();
        }

        $data = $request->validated();

        if ($ransom->getRawOriginal('status') == 'rescued' && $data['status'] != 'rescued') {
            toastr()->error('Essa solicitação já foi resgatada, não podendo ser alterado o status da mesma');
            return redirect()->back();
        }

        if ($data['status'] == 'rescued') {
            $data['rescued_by'] = $user->id;
            $data['withdrawal_request_date'] = Carbon::now();
        }

        $this->donationRepository->update($ransomId, $data);

        if ($request->hasFile('file')) {
            $this->donationRepository->addVoucherImage($ransomId, $request->file('file'));
        }

        toastr()->success('Solicitação de resgate atualizada com sucesso!');
        return redirect()->route($this->route.'index');

    }
}
