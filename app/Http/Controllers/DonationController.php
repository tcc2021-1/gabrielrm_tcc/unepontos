<?php

namespace App\Http\Controllers;

use App\Http\Requests\Admin\DonationRequest;
use App\Repositories\Contracts\DonationRepositoryInterface;
use App\Repositories\Contracts\InstitutionRepositoryInterface;
use App\Repositories\Contracts\WalletRepositoryInterface;
use App\Services\Wallet\Contracts\WalletCacheServiceInterface;
use App\Traits\BreadcrumbTrait;
use Illuminate\Support\Facades\Auth;

class DonationController extends Controller
{
    use BreadcrumbTrait;
    private DonationRepositoryInterface $donationRepository;
    private InstitutionRepositoryInterface $institutionRepository;
    private WalletRepositoryInterface $walletRepository;
    private WalletCacheServiceInterface $walletCacheService;

    private string $route = "donations.";
    private string $viewDir = 'donation.';
    private string $titlePage = 'Doações';

    public array $page = [];

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct(
        DonationRepositoryInterface $donationRepository,
        InstitutionRepositoryInterface $institutionRepository,
        WalletRepositoryInterface $walletRepository,
        WalletCacheServiceInterface $walletCacheService,
    )
    {
        $this->donationRepository = $donationRepository;
        $this->institutionRepository = $institutionRepository;
        $this->walletRepository = $walletRepository;
        $this->walletCacheService = $walletCacheService;

        $this->page = [
            'titlePage' => $this->titlePage,
            'route' => $this->route,
            'viewDir' => $this->viewDir,
            'breadcrumb' => [
                [
                    'title' => 'Inicio',
                    'url' => route('home')
                ],
                [
                    'title' => $this->titlePage,
                    'url' => null
                ],
            ]
        ];



    }

    /**
     * Show the application admin.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        if (! $user->can('donation-list')) {
            toastr()->error('Usuário sem permissão');
            return redirect()->back();
        }

        $data = [];

        if ($user->hasRole('admin')) {
            $data = $this->donationRepository->getAllDonations();
        }

        if ($user->hasRole('institution') && isset($user->institution->id)) {
            $data = $this->donationRepository->getDonationsByInstitutionId($user->institution->id);
        }

        return view($this->viewDir.'index')->with(['page' => $this->page, 'data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        $user = Auth::user();
        if (! $user->can('donation-create')) {
            toastr()->error('Usuário sem permissão');
            return redirect()->back();
        }

        $data = [];

        if ($user->hasRole('admin')) {
            $data['institutions'] = $this->institutionRepository->getInstitutionsPluckIdAndName();
        }

        if (! $user->hasRole('admin') && ! isset($user->institution->id)) {
            toastr()->warning('Usuário não vinculado a uma instituição');
            return redirect()->back();
        }

        if ($user->hasRole('institution') && isset($user->institution->id)) {
            $data['institutions'] = $this->institutionRepository->getByAttribute('user_id', $user->id)->with('user')->get()->pluck('fantasy', 'id');
        }

        $this->page['breadcrumb'] = self::getBreadcrumbLevelTwo($this->page['titlePage'], route($this->page['route'].'index'), 'Criar');
        return view($this->viewDir.'create')->with([
            'page'=>$this->page, 'data' => $data
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param DonationRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(DonationRequest $request)
    {
        $user = Auth::user();
        if (! $user->can('donation-create')) {
            toastr()->error('Usuário sem permissão');
            return redirect()->back();
        }

        $data = $request->validated();

        if (! $user->hasRole('institution') && ! $user->hasRole('admin')) {
            toastr()->warning('Usuário não vinculado aos papéis');
            return redirect()->back();
        }

        if (! $user->hasRole('admin') && isset($user->institution->id) && $user->institution->id != $data['institution_id']) {
            toastr()->warning('Usuário não pode efetuar doações');
            return redirect()->back();
        }

        $availableBalance = $this->walletCacheService->getSumAvailableBalanceByUser($user->id);
        if (empty($availableBalance) || $data['points'] > $availableBalance) {
            toastr()->error('Saldo indisponível para concluir doação');
            return redirect()->back();
        }

        if ($this->walletRepository->userDonation($user->id, $data['points'])) {
            $data['user_id'] = $user->id;
            $this->donationRepository->createDonation($data);
            $this->walletCacheService->forgetSumAvailableBalanceByUser($user->id);
            toastr()->success('Doação realizada com sucesso');
            return redirect()->route($this->route.'index');
        }

        toastr()->error('Tivemos desafios para concluir a operação, contate o suporte');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $user = Auth::user();
        if (! $user->can('donation-list')) {
            toastr()->error('Usuário sem permissão');
            return redirect()->back();
        }

        $data = $this->donationRepository->findWithRelationsById($id);

        if (empty($data)) {
            toastr()->warning('Doação não encontrada');
            return redirect()->back();
        }

        $this->page['breadcrumb'] = self::getBreadcrumbLevelTwo($this->page['titlePage'], route($this->page['route'].'index'), 'Visualizar');
        return view($this->viewDir.'show')
            ->with([
                'page'=>$this->page,
                'data'=>$data
            ]);
    }

}
