<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Repositories\Contracts\CompanyRepositoryInterface;
use App\Repositories\Contracts\InstitutionRepositoryInterface;
use App\Repositories\Contracts\UserRepositoryInterface;

class UserAttachOrganizationFormController extends Controller
{
    private UserRepositoryInterface $userRepository;
    private CompanyRepositoryInterface $companyRepository;
    private InstitutionRepositoryInterface $institutionRepository;

    private string $route = "home.";
    private string $viewDir = 'admin.users.attach.';
    private string $titlePage = 'Atribuir usuário - Empresa/ONG';

    public array $page = [];

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct(
        UserRepositoryInterface $userRepository,
        CompanyRepositoryInterface $companyRepository,
        InstitutionRepositoryInterface $institutionRepository,
    )
    {
        $this->middleware('auth');
        $this->page = [
            'titlePage' => $this->titlePage,
            'route' => $this->route,
            'viewDir' => $this->viewDir,
            'breadcrumb' => [
                [
                    'title' => 'Inicio',
                    'url' => route('home')
                ],
                [
                    'title' => $this->titlePage,
                    'url' => null
                ],
            ]
        ];

        $this->middleware('permission:company-list');
        $this->middleware('permission:company-create', ['only' => ['create','store']]);
        $this->middleware('permission:company-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:institution-list');
        $this->middleware('permission:institution-create', ['only' => ['create','store']]);
        $this->middleware('permission:institution-edit', ['only' => ['edit','update']]);

        $this->userRepository = $userRepository;
        $this->companyRepository = $companyRepository;
        $this->institutionRepository = $institutionRepository;

    }

    public function __invoke(User $user)
    {
        if (empty($user)) {
            toastr()->danger('Usuário não encontrado');
            return redirect()->back();
        }

        if (! $user->hasRole('partner') && ! $user->hasRole('institution')) {
            toastr()->error('Usuário não vinculado aos papéis');
            return redirect()->back();
        }

        $data['companies'] = [];
        $data['institutions'] = [];
        $data['user'] = $user;

        if ($user->hasRole('partner')) {
            $data['companies'] = $this->companyRepository->getCompaniesPluckIdAndName();
        }

        if ($user->hasRole('institution')) {
            $data['institutions'] = $this->institutionRepository->getInstitutionsPluckIdAndName();
        }
        return view($this->viewDir.'edit')->with(['page' => $this->page, 'data' => $data]);
    }

}
