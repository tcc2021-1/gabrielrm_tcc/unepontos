<?php
namespace App\Http\Controllers\Admin;
use App\Http\Requests\Admin\RoleRequest;
use App\Models\Menu;
use App\Traits\BreadcrumbTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;
use Illuminate\Support\Facades\Auth;

class RoleController extends Controller
{
    use BreadcrumbTrait;
    private string $route = "roles.";
    private string $viewDir = 'admin.roles.';
    private string $titlePage = 'Regras';

    public array $page = [];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->page = [
            'titlePage' => $this->titlePage,
            'route' => $this->route,
            'viewDir' => $this->viewDir
        ];

    	$this->middleware('permission:role-list');
    	$this->middleware('permission:role-create', ['only' => ['create','store']]);
    	$this->middleware('permission:role-edit', ['only' => ['edit','update']]);
    	$this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Role::orderBy('id','DESC')->get();
        $this->page['breadcrumb'] = self::getBreadcrumbLevelOne($this->titlePage);
        return view($this->viewDir.'index')
            ->with([
                'page'=>$this->page,
                'data'=>$data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['permission'] = Permission::get();
        $this->page['breadcrumb'] = self::getBreadcrumbLevelTwo($this->page['titlePage'], route($this->page['route'].'index'), 'Criar');
        return view($this->viewDir.'create')
            ->with([
                'page'=>$this->page,
                'data' => $data
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(RoleRequest $request)
    {
        $data = $request->all();

        $model = Role::create([
            'name'          => $data['name'],
            'guard_name'    => 'webmaster'
        ]);

        $model->syncPermissions($request->input('permission'));

        toastr()->success('Criado com sucesso!');
        return redirect()->route($this->route.'index');

    }
    /**
     * Display the specified resource.o errou mto gol
     *
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Role::find($id);
        $data['rolePermission'] = Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
            ->where("role_has_permissions.role_id",$id)
            ->get();
        $this->page['breadcrumb'] = self::getBreadcrumbLevelTwo($this->page['titlePage'], route($this->page['route'].'index'), 'Visualizar');
        return view($this->viewDir.'show')
            ->with([
                'page'=>$this->page,
                'data'=>$data
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {

        $data = Role::find($id);

        $data['rolePermissions'] = DB::table("role_has_permissions")
                                ->where("role_has_permissions.role_id",$id)
                                ->pluck('role_has_permissions.permission_id')
                                ->all();

        $data['permission'] = Permission::get();
        $this->page['breadcrumb'] = self::getBreadcrumbLevelTwo($this->page['titlePage'], route($this->page['route'].'index'), 'Editar');
        return view($this->viewDir.'edit')
            ->with([
                'page'=>$this->page,
                'data'=>$data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {

        $data = $request->all();

        $data['valor_estimado'] = str_replace('.', '' , $request->valor_estimado);
        $data['valor_estimado'] = str_replace(',', '.' , $request->valor_estimado);

        $this->validate($request, [
            'name' => 'required',
            'permission' => 'required',
        ]);

        $update = Role::find($id);
        $update->name = $request->input('name');
        $update->save();
        $update->syncPermissions($request->input('permission'));

        toastr()->success('Atualizado com sucesso!');
        return redirect()->route($this->route.'index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Role::find($id)->delete();
        toastr()->warning('Excluído com sucesso!');
        return redirect()->route($this->route.'index')
            ->with('success', $this->titlePage.' excluida com sucesso!');
    }
}
