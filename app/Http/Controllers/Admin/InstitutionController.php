<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\InstitutionRequest;
use App\Http\Requests\Admin\InstitutionUpdateRequest;
use App\Models\Institution;
use App\Repositories\Contracts\DonationRepositoryInterface;
use App\Repositories\Contracts\InstitutionRepositoryInterface;
use App\Repositories\Contracts\RoleRepositoryInterface;
use App\Repositories\Contracts\WalletRepositoryInterface;
use App\Traits\BreadcrumbTrait;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Response;

class InstitutionController extends Controller
{
    use BreadcrumbTrait;
    private InstitutionRepositoryInterface $institutionRepository;
    private RoleRepositoryInterface $roleRepository;
    private DonationRepositoryInterface $donationRepository;
    private WalletRepositoryInterface $walletRepository;

    private string $route = "institutions.";
    private string $viewDir = 'admin.institution.';
    private string $titlePage = 'Instituição';

    public array $page = [];

    public function __construct(
        InstitutionRepositoryInterface $institutionRepository,
        RoleRepositoryInterface $roleRepository,
        DonationRepositoryInterface $donationRepository,
        WalletRepositoryInterface $walletRepository
    ) {

        $this->institutionRepository = $institutionRepository;
        $this->roleRepository = $roleRepository;
        $this->donationRepository = $donationRepository;
        $this->walletRepository = $walletRepository;

        $this->page = [
            'titlePage' => $this->titlePage,
            'route' => $this->route,
            'viewDir' => $this->viewDir
        ];

        $this->middleware('permission:institution-list');
        $this->middleware('permission:institution-create', ['only' => ['create','store']]);
        $this->middleware('permission:institution-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:institution-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        if ($user->hasRole(['admin'])) {
            $data = $this->institutionRepository->getAll();
            $this->page['breadcrumb'] = self::getBreadcrumbLevelOne($this->titlePage);

            return view($this->viewDir.'index')->with([
                'page'=> $this->page, 'data'=>$data
            ]);
        }

        $data = $this->institutionRepository->paginate(6);
        $this->page['titlePage'] = 'Instituições';
        $this->page['breadcrumb'] = self::getBreadcrumbLevelOne($this->titlePage);

        return view('institutions.index')->with([
            'page'=>$this->page, 'data'=>$data
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        $user = Auth::user();
        if (! $user->hasRole(['admin'])) {
            toastr()->error('Usuário sem permissão');
            return redirect()->back();
        }

        $this->page['breadcrumb'] = self::getBreadcrumbLevelTwo($this->page['titlePage'], route($this->page['route'].'index'), 'Criar');
        return view($this->viewDir.'create')->with([
            'page'=>$this->page, 'data' => []
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param InstitutionRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(InstitutionRequest $request)
    {
        $user = Auth::user();
        if (! $user->hasRole(['admin'])) {
            toastr()->error('Usuário sem permissão');
            return redirect()->back();
        }

        $data = $request->validated();
        $institution = $this->institutionRepository->create($data);
        if ($request->hasFile('file')) {
            $this->institutionRepository->addCoverImage($institution->id, $request->file('file'));
        }

        toastr()->success(__('messages.successfully_created'));
        return redirect()->route($this->route.'index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $data['institution'] = $this->institutionRepository->find($id);

        $this->page['breadcrumb'] = self::getBreadcrumbLevelTwo($this->page['titlePage'], route($this->page['route'] . 'index'), 'Visualizar');
        if ($user->hasRole(['admin'])) {
            return view($this->viewDir . 'show')
                ->with([
                    'page' => $this->page,
                    'data' => $data['institution']
                ]);
        }

        $data['donations'] = $this->donationRepository->getLastDonationsByInstitutionId($id);
        $data['availablePoints'] = $this->walletRepository->sumAvailableBalance($user->id);
        return view('institutions.show')
            ->with([
                'page' => $this->page,
                'data' => $data
            ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        if (! $user->hasRole(['admin', 'institution'])) {
            toastr()->error('Usuário sem permissão');
            return redirect()->back();
        }

        $data = $this->institutionRepository->find($id);
        if (! $user->hasRole(['admin']) && $user->id != $data->user_id ) {
            toastr()->error('Usuário não vinculado');
            return redirect()->back();
        }

        $this->page['breadcrumb'] = self::getBreadcrumbLevelTwo($this->page['titlePage'], route($this->page['route'].'index'), 'Editar');
        return view($this->viewDir.'edit')
            ->with([
                'page'=>$this->page,
                'data'=>$data
            ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param InstitutionUpdateRequest $request
     * @param Institution $institution
     * @return \Illuminate\Http\Response
     */
    public function update(InstitutionUpdateRequest $request, Institution $institution)
    {
        $user = Auth::user();
        if (! $user->hasRole(['admin', 'institution'])) {
            toastr()->error('Usuário sem permissão');
            return redirect()->back();
        }

        $data = $request->validated();
        if (! $user->hasRole(['admin'])) {
            if ($user->id != $institution->user_id) {
                toastr()->error('Usuário não vinculado');
                return redirect()->back();
            }
            $data = Arr::except($data, ['name', 'cnpj']);
        }

        $this->institutionRepository->update($institution->id, $data);

        if ($request->hasFile('file')) {
            $this->institutionRepository->addCoverImage($institution->id, $request->file('file'));
        }

        toastr()->success('Atualizado com sucesso!');
        return redirect()->route($this->route.'show', $institution->id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        if (! $user->hasRole(['admin'])) {
            toastr()->error('Usuário sem permissão');
            return redirect()->back();
        }

        if  ($this->institutionRepository->delete($id)) {
            toastr()->warning('Excluído com sucesso!');
            return redirect()->route($this->route.'index');
        }

        toastr()->error('Erro ao excluir!');
        return redirect()->route($this->route.'index');

    }
}
