<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\MenuRequest;
use App\Repositories\Contracts\MenuRepositoryInterface;
use App\Traits\BreadcrumbTrait;
use Spatie\Permission\Models\Role;
use App\Models\Menu;
use Response;

class MenuController extends Controller
{
    use BreadcrumbTrait;
    private MenuRepositoryInterface $menuRepository;

    private string $route = "menus.";
    private string $viewDir = 'admin.menu.';
    private string $titlePage = 'Menu';

    public array$page = [];

    /**
     * DepartmentController constructor.
     * @param MenuRepositoryInterface $menuRepository
     */
    public function __construct(MenuRepositoryInterface $menuRepository) {

        $this->menuRepository = $menuRepository;
        $this->page = [
            'titlePage' => $this->titlePage,
            'route' => $this->route,
            'viewDir' => $this->viewDir
        ];

        $this->middleware('permission:menu-list');
        $this->middleware('permission:menu-create', ['only' => ['create','store']]);
        $this->middleware('permission:menu-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:menu-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->menuRepository->getAllSortTitle();
        $this->page['breadcrumb'] = self::getBreadcrumbLevelOne($this->titlePage);

        return view($this->viewDir.'index')->with([
            'page'=>$this->page, 'data'=>$data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['menus'] = $this->menuRepository->getOptionsParent();
        $data['all_roles'] = Role::selectRaw('id,name')->get();
        $this->page['breadcrumb'] = self::getBreadcrumbLevelTwo($this->page['titlePage'], route($this->page['route'].'index'), 'Criar');

        return view($this->viewDir.'create')->with([
            'page'=>$this->page, 'data' => $data
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MenuRequest $request)
    {
        $data = $request->all();
        $model = $this->menuRepository->create($data);
        $model->roles()->sync($data['role_id']);

        toastr()->success('Criado com sucesso!');
        return redirect()->route($this->route.'index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->menuRepository->findOrFail($id);
        $this->page['breadcrumb'] = self::getBreadcrumbLevelTwo($this->page['titlePage'], route($this->page['route'].'index'), 'Visualizar');

        return view($this->viewDir.'show')
            ->with([
                'page'=>$this->page,
                'data'=>$data
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data = $this->menuRepository->findWithRoles($id);

        $data['menus'] = $this->menuRepository->getOptionsParent();
        $data['menus-selected'] = $data->parent_id ?? 0;

        $data['all_roles'] = Role::selectRaw('id,name')->get();
        $selected = false;

        foreach ($data['all_roles'] as $key => $obj) {
            foreach($data->roles as $r) {
                if ($r->id == $obj->id) {
                    $selected = true;
                    break;
                }
            }

            $data['all_roles'][$key]->selected  = $selected ? true : false;
            $selected = false;
        }

        $this->page['breadcrumb'] = self::getBreadcrumbLevelTwo($this->page['titlePage'], route($this->page['route'].'index'), 'Editar');

        return view($this->viewDir.'edit')
            ->with([
                'page'=>$this->page,
                'data'=>$data
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MenuRequest $request, Menu $menu)
    {
        $data = $request->all();

        $model = $this->menuRepository->findOrFail($menu->id);

        $model->update($data);
        $model->roles()->sync($data['role_id']);

        toastr()->success('Atualizado com sucesso!');
        return redirect()->route($this->route.'index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Menu::find($id)->delete()) {
            toastr()->warning('Excluído com sucesso!');
            return redirect()->route($this->route.'index');
        }

        toastr()->error('Erro ao excluir!');
        return redirect()->route($this->route.'index');
    }
}
