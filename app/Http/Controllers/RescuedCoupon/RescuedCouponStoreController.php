<?php

namespace App\Http\Controllers\RescuedCoupon;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\RescuedCouponRequest;
use App\Repositories\Contracts\CouponRepositoryInterface;
use App\Repositories\Contracts\WalletRepositoryInterface;
use App\Services\Wallet\Contracts\WalletCacheServiceInterface;
use Illuminate\Support\Facades\Auth;

class RescuedCouponStoreController extends Controller
{
    private CouponRepositoryInterface $couponRepository;
    private WalletRepositoryInterface $walletRepository;
    private WalletCacheServiceInterface $walletCacheService;

    private string $route = "home.";
    private string $viewDir = 'rescued-coupon.';
    private string $titlePage = 'Resgatar cupom';

    public array $page = [];

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct(
        CouponRepositoryInterface $couponRepository,
        WalletRepositoryInterface $walletRepository,
        WalletCacheServiceInterface $walletCacheService,
    )
    {
        $this->middleware('auth');

        $this->couponRepository = $couponRepository;
        $this->walletRepository = $walletRepository;
        $this->walletCacheService = $walletCacheService;

        $this->page = [
            'titlePage' => $this->titlePage,
            'route' => $this->route,
            'viewDir' => $this->viewDir,
            'breadcrumb' => [
                [
                    'title' => 'Inicio',
                    'url' => route('home')
                ],
                [
                    'title' => $this->titlePage,
                    'url' => null
                ],
            ]
        ];



    }

    public function __invoke(RescuedCouponRequest $request)
    {
        $user = Auth::user();
        $data = $request->validated();

        $coupon = $this->couponRepository->getByAttribute('code', $data['code'])->first();

        if (empty($coupon)) {
            toastr()->error('Cupom não encontrado');
            return redirect()->back();
        }

        if ($coupon->rescued) {
            toastr()->error('Cupom já resgatado');
            return redirect()->back();
        }

       if ($this->walletRepository->rescuedCoupon($user->id, $coupon->points)) {
           $rescued['rescued'] = true;
           $rescued['rescued_by'] = $user->id;

           $this->couponRepository->update($coupon->id, $rescued);
           $this->walletCacheService->forgetSumAvailableBalanceByUser($user->id);
           toastr()->success('Cupom resgatado com sucesso');
           return redirect()->route('home');
       }

        toastr()->error('Tivemos desafios para resgatar o cupom informado, contate o suporte');
        return redirect()->back();
    }

}
