<?php

namespace App\Http\Controllers\RescuedCoupon;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Repositories\Contracts\UserRepositoryInterface;

class RescuedCouponFormController extends Controller
{
    private UserRepositoryInterface $userRepository;

    private string $route = "home.";
    private string $viewDir = 'rescued-coupon.';
    private string $titlePage = 'Resgatar cupom';

    public array $page = [];

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct(
        UserRepositoryInterface $userRepository,
    )
    {
        $this->middleware('auth');
        $this->page = [
            'titlePage' => $this->titlePage,
            'route' => $this->route,
            'viewDir' => $this->viewDir,
            'breadcrumb' => [
                [
                    'title' => 'Inicio',
                    'url' => route('home')
                ],
                [
                    'title' => $this->titlePage,
                    'url' => null
                ],
            ]
        ];

        $this->userRepository = $userRepository;

    }

    public function __invoke(User $user)
    {
        if (empty($user)) {
            toastr()->danger('Usuário não encontrado');
            return redirect()->back();
        }

        $data['user'] = $user;

        return view($this->viewDir.'create')->with(['page' => $this->page, 'data' => $data]);
    }

}
