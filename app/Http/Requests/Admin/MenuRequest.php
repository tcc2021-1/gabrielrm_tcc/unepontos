<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class MenuRequest extends FormRequest
{
    protected function prepareForValidation()
    {
        if ($this->parent_id == null) {
            $this->merge(['parent_id' => 0]);
        }
        if ($this->order == null) {
            $this->merge(['order' => 0]);
        }
        if ($this->enabled == null) {
            $this->merge(['enabled' => 0]);
        }
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'     => 'required|min:2|max:30',
            'icon'      => 'required|min:2',
            'url'       => ['nullable',Rule::unique('menus','url')->ignore($this->menu)],
            'role_id'   => 'required',
        ];

    }
}
