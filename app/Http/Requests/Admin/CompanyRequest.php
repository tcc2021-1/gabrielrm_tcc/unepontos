<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fantasy' => 'required|min:5|max:200',
            'company_name' => 'required|min:5|max:200',
            'cnpj' => 'required|string|min:14|max:14|unique:companies,cnpj',
            'file' => 'nullable|image',
            'description' => 'required|min:5|max:1500',
        ];
    }
}
