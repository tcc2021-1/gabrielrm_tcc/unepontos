<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CompanyUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (Auth::user()->hasRole(['admin'])) {
            return [
                'fantasy' => 'required|min:5|max:200',
                'company_name' => 'required|min:5|max:200',
                'cnpj' => "required|string|min:14|max:14|unique:companies,cnpj,{$this->company->id}",
                'file' => 'nullable|image',
                'description' => 'required|min:5|max:2500',
            ];
        }

        return [
            'file' => 'nullable|image',
            'description' => 'required|min:5|max:2500',
        ];

    }
}
