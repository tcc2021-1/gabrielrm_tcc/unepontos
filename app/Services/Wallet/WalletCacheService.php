<?php

namespace App\Services\Wallet;

use App\Repositories\Contracts\WalletRepositoryInterface;
use App\Services\Wallet\Contracts\WalletCacheServiceInterface;
use Illuminate\Support\Facades\Cache;

class WalletCacheService implements WalletCacheServiceInterface
{
    private WalletRepositoryInterface $walletRepository;

    public function __construct(WalletRepositoryInterface $walletRepository)
    {
        $this->walletRepository = $walletRepository;
    }

    /**
     * @param int $userId
     * @return int
     */
    public function getSumAvailableBalanceByUser(int $userId): int
    {
        return Cache::remember('sum_available_balance_by_'. $userId .'_user_id', 60, function () use ($userId) {
            return $this->walletRepository->sumAvailableBalance($userId);
        });
    }

    /**
     * @param int $userId
     * @return bool
     */
    public function forgetSumAvailableBalanceByUser(int $userId): bool
    {
        return Cache::forget('sum_available_balance_by_'. $userId .'_user_id');
    }

}
