<?php

namespace App\Providers;

use App\Services\Wallet\Contracts\WalletCacheServiceInterface;
use App\Services\Wallet\WalletCacheService;
use Illuminate\Support\ServiceProvider;

class WalletCacheServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            WalletCacheServiceInterface::class, WalletCacheService::class
        );
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [WalletCacheServiceInterface::class];
    }
}
