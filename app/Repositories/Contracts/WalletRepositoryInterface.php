<?php

namespace App\Repositories\Contracts;


interface WalletRepositoryInterface
{
    /**
     * @param int $userId
     * @param int $points
     * @return mixed
     */
    public function rescuedCoupon(int $userId, int $points);

    /**
     * @param int $userId
     * @return mixed
     */
    public function sumAvailableBalance(int $userId);

    /**
     * @param int $userId
     * @param int $points
     * @return mixed
     */
    public function userDonation(int $userId, int $points);
}
