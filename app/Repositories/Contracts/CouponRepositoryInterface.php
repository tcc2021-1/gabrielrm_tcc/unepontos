<?php

namespace App\Repositories\Contracts;


interface CouponRepositoryInterface
{
    /**
     * @param int $companyId
     * @param int $limit
     * @return mixed
     */
    public function getLastCouponsRescuedByCompanyId(int $companyId, int $limit = 8);

    /**
     * @param int $companyId
     * @return mixed
     */
    public function getSumPointsLastSixMonthsGroupMonth(int $companyId);

    /**
     * @return int
     */
    public function countCouponsCreated(): int;

    /**
     * @return int
     */
    public function countCouponsRescued(): int;

    /**
     * @param int $limit
     * @return mixed
     */
    public function getLastCoupons(int $limit = 5): mixed;

    /**
     * @return mixed
     */
    public function getSumPointsLastMonthsGroupMonth(): mixed;
}
