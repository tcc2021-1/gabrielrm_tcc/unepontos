<?php

namespace App\Repositories\Contracts;


interface InstitutionRepositoryInterface
{
    public function getInstitutionsPluckIdAndName();

    /**
     * @param int $id
     * @param $file
     * @return mixed
     */
    public function addCoverImage(int $id, $file);
}
