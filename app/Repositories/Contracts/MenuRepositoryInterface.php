<?php

namespace App\Repositories\Contracts;


interface MenuRepositoryInterface
{
   public function getAllSortTitle($sort);
   public function getOptions();
   public function getOptionsParent();
   public function findWithRoles(int $id);
}
