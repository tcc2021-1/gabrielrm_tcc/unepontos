<?php

namespace App\Repositories\Contracts;


interface DonationRepositoryInterface
{
    /**
     * @param array $data
     * @return mixed
     */
    public function createDonation(array $data);

    /**
     * @param int $id
     * @return mixed
     */
    public function findWithRelationsById(int $id);

    /**
     * @param int $institutionId
     * @param int $limit
     * @return mixed
     */
    public function getLastDonationsByInstitutionId(int $institutionId, int $limit = 8);

    /**
     * @return mixed
     */
    public function getAllDonations();

    /**
     * @param int $institutionId
     * @return mixed
     */
    public function getDonationsByInstitutionId(int $institutionId);

    /**
     * @param int $institutionId
     * @return mixed
     */
    public function getRansomRequestsByInstitutionId(int $institutionId);

    /**
     * @return mixed
     */
    public function getAllRansomRequests();

    /**
     * @param int $userId
     * @param int $institutionId
     * @return bool
     */
    public function createRequestRansom(int $userId, int $institutionId);

    /**
     * @param int $institutionId
     * @return mixed
     */
    public function getSumPointsLastSixMonthsGroupMonthByInstitutionId(int $institutionId);

    /**
     * @param int $institutionId
     * @return int
     */
    public function sumAvailablePoints(int $institutionId): int;

    /**
     * @param int $id
     * @param $file
     * @return mixed
     */
    public function addVoucherImage(int $id, $file);

    /**
     * @return int
     */
    public function countDonationsByLastMonths(): int;

    /**
     * @param int $limit
     * @return mixed
     */
    public function getLastDonations(int $limit = 6): mixed;

    /**
     * @return mixed
     */
    public function getSumPointsLastMonthsGroupMonth(): mixed;
}
