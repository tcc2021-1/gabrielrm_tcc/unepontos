<?php

namespace App\Repositories;

use App\Models\Donation;
use App\Repositories\Contracts\DonationRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class DonationRepository extends Repository implements DonationRepositoryInterface
{
    const DONATIONS_RELATIONS = ['user', 'institution', 'rescued'];

    protected $model;

    public function __construct(Donation $model)
    {
        $this->model = $model;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function createDonation(array $data)
    {
        $data['expiration_date'] = Carbon::now()->addMonths($this->model::MONTHS_TO_EXPIRE);
        return $this->model->create($data);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function findWithRelationsById(int $id)
    {
        return $this->model->where('id', $id)->with(self::DONATIONS_RELATIONS)->first();
    }

    /**
     * @return mixed|void
     */
    public function getAllDonations()
    {
        return $this->model->donationType()
            ->with(self::DONATIONS_RELATIONS)
            ->get();
    }

    /**
     * @param int $institutionId
     * @return mixed|void
     */
    public function getDonationsByInstitutionId(int $institutionId)
    {
        return $this->model->donationType()
            ->where('institution_id', $institutionId)
            ->with(self::DONATIONS_RELATIONS)
            ->get();
    }

    /**
     * @param int $institutionId
     * @param int $limit
     * @return mixed
     */
    public function getLastDonationsByInstitutionId(int $institutionId, int $limit = 8)
    {
        return $this->model->where('institution_id', $institutionId)
                    ->donationType()
                    ->with('user:id,name')
                    ->orderBy('id', 'desc')
                    ->limit($limit)
                    ->get();
    }

    /**
     * @param int $institutionId
     * @return mixed|void
     */
    public function getRansomRequestsByInstitutionId(int $institutionId)
    {
        return $this->model->where('institution_id', $institutionId)
            ->where('type', $this->model::RESCUE_TYPE)
            ->with('user:id,name')
            ->orderBy('id', 'desc')
            ->get();
    }

    /**
     * @return mixed
     */
    public function getAllRansomRequests()
    {
        return $this->model->where('type', $this->model::RESCUE_TYPE)
            ->with('user:id,name')
            ->orderBy('id', 'desc')
            ->get();
    }

    /**
     * @param int $userId
     * @param int $institutionId
     * @return bool
     */
    public function createRequestRansom(int $userId, int $institutionId)
    {
        $availablePoints = $this->availablePointsQuery($institutionId)->get();

        if (empty($availablePoints)) {
            return false;
        }

        $sumAvailablePoints = $availablePoints->sum('points');

        if (config('une.min_points_request_rescue') > $sumAvailablePoints) {
            return false;
        }

        $donation = new $this->model();
        $donation->user_id = $userId;
        $donation->institution_id = $institutionId;
        $donation->points = $sumAvailablePoints;
        $donation->type = $this->model::RESCUE_TYPE;
        $donation->status = $this->model::STATUS['pending'];
        $donation->expiration_date = Carbon::now()->addMonths($this->model::MONTHS_TO_EXPIRE);
        $donation->save();

        $donationsIds = $availablePoints->pluck('id');
        $this->model->where('status', $this->model::STATUS['available'])
                    ->whereIn('institution_id', $donationsIds)
                    ->update([
                        'status' => $this->model::STATUS['rescued'],
                        'withdrawal_request_date' => Carbon::now(),
                        'rescued_by' => $userId,
                    ]);

        return true;
    }

    /**
     * @param int $institutionId
     * @return mixed
     */
    public function getSumPointsLastSixMonthsGroupMonthByInstitutionId(int $institutionId)
    {
        return $this->model->select(DB::raw('SUM(points) as points, CONCAT(MONTH(created_at), "/" , YEAR(created_at)) as date'))
            ->donationType()
            ->where('institution_id', $institutionId)
            ->groupBy('date')
            ->get()
            ->toArray();
    }

    /**
     * @param int $institutionId
     * @return int
     */
    public function sumAvailablePoints(int $institutionId): int
    {
       return $this->availablePointsQuery($institutionId)
                            ->sum('points');
    }

    /**
     * @param int $institutionId
     * @return mixed
     */
    private function availablePointsQuery(int $institutionId)
    {
        return $this->model->where('institution_id', $institutionId)
            ->donationType()
            ->where('status', $this->model::STATUS['available']);
    }

    /**
     * @param int $id
     * @param $file
     * @return mixed
     */
    public function addVoucherImage(int $id, $file)
    {
        return $this->model->find($id)
            ->addMedia($file)
            ->usingFileName(bcrypt(md5(time())))
            ->toMediaCollection('voucher');
    }

    /**
     * @return int
     */
    public function countDonationsByLastMonths(): int
    {
        return $this->model->donationType()
            ->where('created_at', '>=',  $this->getSubMonths($this->model::MONTHS_TO_EXPIRE))
            ->count();
    }

    /**
     * @param int $limit
     * @return mixed
     */
    public function getLastDonations(int $limit = 6): mixed
    {
        return $this->model->donationType()
            ->with('user:id,name')
            ->orderBy('id', 'desc')
            ->limit($limit)
            ->get();
    }

    /**
     * @return mixed
     */
    public function getSumPointsLastMonthsGroupMonth(): mixed
    {
        return $this->model->select(DB::raw('SUM(points) as points, CONCAT(MONTH(created_at), "/" , YEAR(created_at)) as date'))
            ->donationType()
            ->where('created_at', '>=', $this->getSubMonths($this->model::MONTHS_TO_EXPIRE))
            ->groupBy('date')
            ->get()
            ->toArray();
    }

    /**
     * @param int $months
     * @return Carbon
     */
    private function getSubMonths(int $months): Carbon
    {
        return Carbon::now()->subMonths($months);
    }
}
