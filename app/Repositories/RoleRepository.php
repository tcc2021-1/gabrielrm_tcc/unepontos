<?php

namespace App\Repositories;

use App\Repositories\Contracts\RoleRepositoryInterface;
use Spatie\Permission\Models\Role;


class RoleRepository extends Repository implements RoleRepositoryInterface
{
    protected $model;

    public function __construct(Role $role)
    {
        $this->model = $role;
    }

    /**
     * @return mixed
     */
    public function getAllByName()
    {
        return $this->model->select('id','name')->orderBy('name')->get();
    }
}
