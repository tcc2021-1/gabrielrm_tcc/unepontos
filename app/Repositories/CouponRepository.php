<?php

namespace App\Repositories;

use App\Models\Coupon;
use App\Repositories\Contracts\CouponRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CouponRepository extends Repository implements CouponRepositoryInterface
{
    const LAST_MONTHS = 6;

    protected $model;

    public function __construct(Coupon $model)
    {
        $this->model = $model;
    }

    /**
     * @param int $companyId
     * @param int $limit
     * @return mixed
     */
    public function getLastCouponsRescuedByCompanyId(int $companyId, int $limit = 8)
    {
        return $this->model->where('company_id', $companyId)
            ->with('rescuedBy:id,name')
            ->where('rescued', true)
            ->orderBy('id', 'desc')
            ->limit($limit)
            ->get();
    }

    /**
     * @param int $companyId
     * @return mixed
     */
    public function getSumPointsLastSixMonthsGroupMonth(int $companyId)
    {
        return $this->model->select(DB::raw('SUM(points) as points, CONCAT(MONTH(created_at), "/" , YEAR(created_at)) as date'))
            ->where('company_id', $companyId)
            ->groupBy('date')
            ->get()
            ->toArray();
    }

    /**
     * @return int
     */
    public function countCouponsCreated(): int
    {
        return $this->queryLastMonths()->count();
    }

    /**
     * @return int
     */
    public function countCouponsRescued(): int
    {
       return $this->queryLastMonths()
           ->where('rescued', true)
           ->count();
    }

    /**
     * @param int $limit
     * @return mixed
     */
    public function getLastCoupons(int $limit = 5): mixed
    {
        return $this->model->select('id', 'company_id', 'points')
            ->with('company:id,fantasy')
            ->orderBy('id', 'desc')
            ->take($limit)
            ->get();
    }

    /**
     * @return mixed
     */
    public function getSumPointsLastMonthsGroupMonth(): mixed
    {
        return $this->model->select(DB::raw('SUM(points) as points, CONCAT(MONTH(created_at), "/" , YEAR(created_at)) as date'))
            ->where('created_at', '>=', $this->getSubMonths(self::LAST_MONTHS))
            ->groupBy('date')
            ->get()
            ->toArray();
    }

    /**
     * @param int $months
     * @return Carbon
     */
    private function getSubMonths(int $months): Carbon
    {
        return Carbon::now()->subMonths($months);
    }

    /**
     * @return mixed
     */
    private function queryLastMonths() {
        $lastMonths = Carbon::now()->subMonths(self::LAST_MONTHS);
        return $this->model->where('created_at', '>=', $lastMonths);
    }
}
