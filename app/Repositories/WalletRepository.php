<?php

namespace App\Repositories;

use App\Models\Wallet;
use App\Repositories\Contracts\WalletRepositoryInterface;
use Carbon\Carbon;

class WalletRepository extends Repository implements WalletRepositoryInterface
{
    protected $model;

    public function __construct(Wallet $model)
    {
        $this->model = $model;
    }

    /**
     * @param int $userId
     * @param int $points
     * @return mixed
     */
    public function rescuedCoupon(int $userId, int $points)
    {
        $data['user_id'] = $userId;
        $data['points'] = $points;
        $data['expiration_date'] = Carbon::now()->addMonths($this->model::MONTHS_TO_EXPIRE);
        return $this->model->create($data);
    }

    /**
     * @param int $userId
     * @param int $points
     * @return mixed
     */
    public function userDonation(int $userId, int $points)
    {
        $data['user_id'] = $userId;
        $data['points'] = $points;
        $data['expiration_date'] = Carbon::now();
        $data['status'] = $this->model::STATUS['donated'];
        return $this->model->create($data);
    }

    /**
     * @param int $userId
     * @return mixed|void
     */
    public function sumAvailableBalance(int $userId)
    {

        $statusAvailable = [$this->model::STATUS['rescued']];
        $statusNotAvailable = array_diff($this->model::STATUS, $statusAvailable);

        $sumRescued = $this->model->where('user_id', $userId)
                    ->whereIn('status', $statusAvailable)
                    ->sum('points');

        $sumNotAvaiable = $this->model->where('user_id', $userId)
                                ->whereIn('status', $statusNotAvailable)
                                ->sum('points');

        $sumAvailable = $sumRescued - $sumNotAvaiable;

        return $sumAvailable > 0 ? $sumAvailable : 0;
    }
}
