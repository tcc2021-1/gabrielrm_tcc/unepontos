<?php

namespace App\Repositories;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

Abstract class Repository
{
    /**
     * @return mixed
     */
    public function getAll()
    {
        return $this->model->get();
    }

    /**
     * return a model by a ad
     *
     * @param int $id
     * @return Model
     */
    public function getById(int $id)
    {
        $model = $this->model->find($id);

        if (!$model) {
            throw new ModelNotFoundException();
        }

        return $model;
    }

    /**
     * Delete a resource for a given id
     * @param int $id
     * @return
     */
    public function delete(int $id)
    {
        return  $this->model->destroy('id', $id);

    }

    /**
     * paginate set of results per page
     * @param int $perPage
     * @return
     */
    public function paginate(int $perPage = 15)
    {
        return $this->model->paginate($perPage);
    }

    /**
     * Create a new record in database
     * @param array $attributes
     * @return
     */
    public function create(array $attributes)
    {
        return $this->model->create($attributes);
    }

    /**
     * find a record in database
     * @param int $id
     * @return
     */
    public function find(int $id)
    {
        return $this->model->find($id);
    }

    /**
     * update a record in database
     * @param int $id
     * @param array $attributes
     * @return
     */
    public function update(int $id, array $attributes)
    {
        return $this->model->find($id)
            ->fill($attributes)
            ->save();
    }

    /**
     * find a record in database
     * @param int $id
     * @return
     */
    public function findOrFail(int $id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * find a record in database
     * @return
     */
    public function query()
    {
        return $this->model->query();
    }

    /**
     * find a record in database
     * @param string $relation
     * @return mixed
     */
    public function queryWithRelation(string $relation)
    {
        return $this->model->query()->with($relation);
    }

    public function findWithRelations(int $id, array $relations)
    {
        return $this->model->with($relations)->find($id);
    }

    public function getWithRelations(array $relations)
    {
        return $this->model->with($relations)->get();
    }

    public function getByAttribute(string $field, string $attribute)
    {
        return $this->model->where($field, $attribute);
    }

    public function getByAttributeWithRelations(string $field, string $attribute, array $relations)
    {
        return $this->model->with($relations)->where($field, $attribute)->get();
    }

}
