<?php

namespace App\Repositories;

use App\Models\Menu;
use App\Repositories\Contracts\MenuRepositoryInterface;

class MenuRepository extends Repository implements MenuRepositoryInterface
{
    protected $model;

    public function __construct(Menu $model)
    {
        $this->model = $model;
    }

    public function getAllSortTitle($sort = 'asc')
    {
        return $this->model->orderBy('title', $sort)->get();
    }

    public function getOptions()
    {
        return $this->model->options();
    }

    public function getOptionsParent()
    {
        return $this->model->optionsParent();
    }

    public function findWithRoles(int $id)
    {
        return $this->model->where('id' ,$id)->with('roles')->first();
    }
}
