<?php

namespace App\Repositories;

use App\Models\Company;
use App\Repositories\Contracts\CompanyRepositoryInterface;

class CompanyRepository extends Repository implements CompanyRepositoryInterface
{
    protected $model;

    public function __construct(Company $model)
    {
        $this->model = $model;
    }

    public function getCompaniesPluckIdAndName()
    {
        return $this->model->with('user')->get()->pluck('fantasy', 'id');
    }

    /**
     * @param int $id
     * @param $file
     * @return mixed
     */
    public function addCoverImage(int $id, $file)
    {
        return $this->model->find($id)
            ->addMedia($file)
            ->usingFileName(bcrypt(md5(time())))
            ->toMediaCollection('company_cover');
    }
}
