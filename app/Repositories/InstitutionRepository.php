<?php

namespace App\Repositories;

use App\Models\Institution;
use App\Repositories\Contracts\InstitutionRepositoryInterface;

class InstitutionRepository extends Repository implements InstitutionRepositoryInterface
{
    protected $model;

    public function __construct(Institution $model)
    {
        $this->model = $model;
    }

    public function getInstitutionsPluckIdAndName()
    {
        return $this->model->get()->pluck('name', 'id');
    }

    /**
     * @param int $id
     * @param $file
     * @return mixed
     */
    public function addCoverImage(int $id, $file)
    {
        return $this->model->find($id)
            ->addMedia($file)
            ->usingFileName(bcrypt(md5(time())))
            ->toMediaCollection('institution_cover');
    }
}
