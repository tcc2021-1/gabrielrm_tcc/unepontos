<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use OwenIt\Auditing\Contracts\Auditable;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\Permission\Traits\HasRoles;
use Wildside\Userstamps\Userstamps;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class User extends Authenticatable implements HasMedia, Auditable
{
    use HasFactory, Notifiable, HasRoles, InteractsWithMedia, Userstamps, \OwenIt\Auditing\Auditable;
    use SoftDeletes;

    protected $guard_name = 'webmaster';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function company()
    {
        return $this->hasOne('App\Models\Company');
    }

    public function institution()
    {
        return $this->hasOne('App\Models\Institution');
    }

    public function wallet()
    {
        return $this->hasOne('App\Models\Wallet');
    }

    public function donation()
    {
        return $this->hasOne('App\Models\Donation');
    }

    public function getLastMedia(string $collectionName = 'default', $filters = []): ?Media
    {
        $media = $this->getMedia($collectionName, $filters);

        return $media->last();
    }

    public function getLastImageUrl(string $collectionName = 'default', string $conversionName = ''): string
    {
      $media = $this->getLastMedia($collectionName);

      if (! $media) {
          return $this->getFallbackMediaUrl($collectionName) ?: '';
      }

      if ($conversionName !== '' && ! $media->hasGeneratedConversion($conversionName)) {
          return $media->getUrl();
      }

      return $media->getUrl($conversionName);
    }
}
