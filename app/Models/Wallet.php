<?php


namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\Model;

class Wallet extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    use SoftDeletes;

    const STATUS = [
        'rescued' => 'rescued',
        'donated' => 'donated',
        'expired' => 'expired',
        'suspended' => 'suspended',
        'cancelled' => 'cancelled',
    ];

    const MONTHS_TO_EXPIRE = 6;

    protected $fillable = [
        'id',
        'user_id',
        'points',
        'status',
        'expiration_date',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
