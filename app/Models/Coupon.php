<?php


namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model implements Auditable
{
    use HasFactory, \OwenIt\Auditing\Auditable;
    use SoftDeletes;

    protected $fillable = [
        'id',
        'user_id',
        'company_id',
        'rescued_by',
        'code',
        'points',
        'rescued',
    ];

    /**
     * @return void
     */
    public function getCreatedAtAttribute($value)
    {
       return (new Carbon($value))->format('d/m/y - H:i:s');
    }

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function rescuedBy()
    {
        return $this->hasOne('App\Models\User', 'id', 'rescued_by');
    }

    public function company()
    {
        return $this->hasOne('App\Models\Company', 'id', 'company_id');
    }
}
