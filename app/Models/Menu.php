<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class Menu extends Model
{
    use HasRoles;

    protected $parent = 'parent_id';

    protected $fillable = [
        'title', 'icon', 'slug', 'url', 'parent_id', 'order', 'enabled'
    ];

    public function parent()
    {
        return $this->hasOne('App\Models\Menu', 'id', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Menu', 'parent_id', 'id');
    }

    public static function options()
    {
        return static::orderBy('title')->pluck('title', 'id')->toArray();
    }

    public static function optionsParent()
    {
        return static::orderBy('title')->whereNull('url')->pluck('title', 'id')->toArray();
    }

    public static function selected($id)
    {
        return self::where('id', $id)->pluck('id')->first();
    }

    public static function tree()
    {
        return static::with(implode('.', array_fill(0, 100, 'children')))->where('parent_id', '=', '0')->orderBy('id')->get();
    }

    public function roles()
    {
        return $this->belongsToMany("Spatie\Permission\Models\Role","menu_roles" )->withPivot('role_id');
    }

}
