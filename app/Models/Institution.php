<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Institution extends Model implements HasMedia, Auditable
{
    use HasFactory, \OwenIt\Auditing\Auditable;
    use InteractsWithMedia;
    use SoftDeletes;

    protected $fillable = [
        'id',
        'user_id',
        'cnpj',
        'name',
        'description',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function donation()
    {
        return $this->hasOne('App\Models\Donation');
    }

    public function getLastMedia(string $collectionName = 'default', $filters = []): ?Media
    {
        $media = $this->getMedia($collectionName, $filters);

        return $media->last();
    }

    public function getLastImageUrl(string $collectionName = 'default', string $conversionName = ''): string
    {
        $media = $this->getLastMedia($collectionName);

        if (! $media) {
            return $this->getFallbackMediaUrl($collectionName) ?: '';
        }

        if ($conversionName !== '' && ! $media->hasGeneratedConversion($conversionName)) {
            return $media->getUrl();
        }

        return $media->getUrl($conversionName);
    }

    public function getCoverImageAttribute() {
        return count($this->media) ? $this->media->first()->getUrl() : asset('/images/sem-image.png');;
    }

    public function setDescriptionAttribute($value)
    {
        $this->attributes['description'] = nl2br(strip_tags(e(trim($value))));
    }

    protected static function booted()
    {
        static::addGlobalScope('cover', function (Builder $builder) {
            $builder->with(['media' => function ($query) {
                $query->where('collection_name', 'institution_cover')
                    ->orderBy('id', 'desc')
                    ->first();
            }]);
        });
    }

}
