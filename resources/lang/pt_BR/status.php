<?php
return [
    'rescued' => 'Resgatado',
    'pending' => 'Pendente',
    'available' => 'Disponível',
    'expired' => 'Expirado',
    'suspended' => 'Suspendido',
    'cancelled' => 'Cancelado'
];
