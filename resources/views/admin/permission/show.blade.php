<x-admin title="{{ $page['titlePage'] }}" :breadcrumbs="$page['breadcrumb']">
    <x-admin.internal-card>
        <x-show.form-group description="Suerior:">
            <p class="form-control-static">
                {{$data->father}}
            </p>
        </x-show.form-group>
        <x-show.form-group description="Descrição:">
            <p class="form-control-static">
                {{$data->description}}
            </p>
        </x-show.form-group>
        <x-show.form-group description="Nome:">
            <p class="form-control-static">
                {{$data->name}}
            </p>
        </x-show.form-group>

        <x-buttons.form-action
            routeBack="{{ route($page['route'].'index') }}"
            routeAction="{{ route($page['route'].'edit',$data->id) }}"
            descriptionAction="Editar"
            iconAction="fa fa-edit"
        />

    </x-admin.internal-card>

</x-admin>
