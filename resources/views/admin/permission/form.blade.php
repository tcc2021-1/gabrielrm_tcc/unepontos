{{ csrf_field()}}
<x-form.input
    type="text"
    description="Superior"
    name="father"
    value="{{ $data['father'] ?? null  }}"
    errorMessage="{{ $errors->first('father') }}"
/>
<x-form.input
    type="text"
    description="Descrição"
    name="description"
    value="{{ $data['description'] ?? null }}"
    errorMessage="{{ $errors->first('description') }}"
/>
<x-form.input
    type="text"
    description="Permissão"
    name="name"
    value="{{ $data['name'] ?? null }}"
    errorMessage="{{ $errors->first('name') }}"
/>
<x-buttons.form-action
    routeBack="{{ route($page['route'].'index') }}"
    descriptionAction="Salvar"
    iconAction="fa fa-check"
    submit="true"
/>
