<x-admin title="{{ $page['titlePage'] }}" :breadcrumbs="$page['breadcrumb']">
    <x-admin.internal-card>
        <x-show.form-group description="CNPJ:">
            <p class="form-control-static">
                {{ $data['company']->cnpj }}
            </p>
        </x-show.form-group>

        <x-show.form-group description="Nome fantasia:">
            <p class="form-control-static">
                {{ $data['company']->fantasy }}
            </p>
        </x-show.form-group>

        <x-show.form-group description="Razão social:">
            <p class="form-control-static">
                {{ $data['company']->company_name }}
            </p>
        </x-show.form-group>
        <x-show.form-group description="Capa da empresa:">
            <img src="{{ $data['company']->coverImage }}" class="img elevation-2" style="max-height:400px;"  alt="Capa da empresa">
        </x-show.form-group>
        <br>
        <x-show.form-group description="Descrição:">
            <p class="form-control-static">
                {!! $data['company']->description !!}
            </p>
        </x-show.form-group>

        <x-buttons.form-action
            routeBack="{{ route($page['route'].'index') }}"
            routeAction="{{ route($page['route'].'edit',$data['company']->id) }}"
            descriptionAction="Editar"
            iconAction="fa fa-edit"
        />

    </x-admin.internal-card>
</x-admin>
