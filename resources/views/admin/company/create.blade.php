<x-admin title="{{ $page['titlePage'] }}" :breadcrumbs="$page['breadcrumb']">
    <x-admin.internal-card>
        <x-form route="{{ route($page['route'].'store') }}" method="POST" enctype="multipart/form-data">
            @include( $page['viewDir'].'form')
        </x-form>
    </x-admin.internal-card>
    @push('scripts')
        @toastr_render
    @endpush
</x-admin>
