{{ csrf_field()}}
@role('admin')
    <x-form.input
        type="text"
        description="Nome fantasia"
        name="fantasy"
        value="{{ $data['fantasy'] ?? null  }}"
        errorMessage="{{ $errors->first('fantasy') }}"
    />
    <x-form.input
        type="text"
        description="Razão social"
        name="company_name"
        value="{{ $data['company_name'] ?? null  }}"
        errorMessage="{{ $errors->first('company_name') }}"
    />
    <x-form.input
        type="text"
        description="CNPJ"
        name="cnpj"
        value="{{ $data['cnpj'] ?? null  }}"
        errorMessage="{{ $errors->first('cnpj') }}"
    />
@else
    <x-form.input-disabled
        type="text"
        description="Nome fantasia"
        name="fantasy"
        value="{{ $data['fantasy'] ?? null  }}"
        errorMessage="{{ $errors->first('fantasy') }}"
    />
    <x-form.input-disabled
        type="text"
        description="Razão social"
        name="company_name"
        value="{{ $data['company_name'] ?? null  }}"
        errorMessage="{{ $errors->first('company_name') }}"
    />
    <x-form.input-disabled
        type="text"
        description="CNPJ"
        name="cnpj"
        value="{{ $data['cnpj'] ?? null  }}"
        errorMessage="{{ $errors->first('cnpj') }}"
    />
@endrole

<x-form.input-file
    description="Imagem de Capa"
    name="file"
    value=""
    errorMessage="{{ $errors->first('file') }}"
/>
<x-form.text-area
    type="text"
    description="Descrição"
    name="description"
    value="{!! isset($data['description']) ? $data['description'] : null  !!}"
    errorMessage="{{ $errors->first('description') }}"
/>


<x-buttons.form-action
    routeBack="{{ route($page['route'].'index') }}"
    descriptionAction="Salvar"
    iconAction="fa fa-check"
    submit="true"
/>


