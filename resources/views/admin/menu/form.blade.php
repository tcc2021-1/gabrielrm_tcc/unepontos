{{ csrf_field()}}
<x-form.input
    type="text"
    description="Título"
    name="title"
    value="{{ $data['title'] ?? null  }}"
    errorMessage="{{ $errors->first('title') }}"
/>
<x-form.input
    type="text"
    description="URL"
    name="url"
    value="{{ $data['url'] ?? null  }}"
    errorMessage="{{ $errors->first('url') }}"
/>
<x-form.input
    type="text"
    description="Ordem"
    name="order"
    value="{{ $data['order'] ?? null  }}"
    errorMessage="{{ $errors->first('order') }}"
/>
<x-form.select
    name="parent_id"
    description="Superior"
    :values="$data['menus']"
    valueSelected="{{ $data->parent_id ?? null  }}"
    errorMessage="{{ $errors->first('parent_id') }}"
/>

<x-form.select-multiple
    description="Regras"
    name="role_id"
    :values="$data['all_roles']"
    errorMessage="{{ $errors->first('role_id') }}"
/>
<x-form.icon-picker
    name="icon"
    description="Ìcone"
    value="{{ $data['icon'] ?? null  }}"
    valueSelected="{{ $data->icon ?? null  }}"
    errorMessage="{{ $errors->first('icon') }}"
/>
<x-buttons.form-action
    routeBack="{{ route($page['route'].'index') }}"
    descriptionAction="Salvar"
    iconAction="fa fa-check"
    submit="true"
/>
@push('style')
    <link rel="stylesheet" href="{{ mix('css/fontawesome-iconpicker.css') }}">
@endpush
@push('scripts')
    <script src="{{ mix('js/fontawesome-iconpicker.js') }}"></script>
    @toastr_render
@endpush
