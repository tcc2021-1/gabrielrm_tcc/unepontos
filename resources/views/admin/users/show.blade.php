<x-admin title="{{ $page['titlePage'] }}" :breadcrumbs="$page['breadcrumb']">
    <x-admin.internal-card>
        <x-show.form-group description="Imagem:">
            <p class="form-control-static">
                <img width="60px" height="60px" src="@if(count($data->media)) {{$data->getFirstMediaUrl('profile')}} @else {{asset('images/default-user.png')}} @endif" class="img-circle elevation-2"
                     alt="Foto Usuário">
            </p>
        </x-show.form-group>
        <x-show.form-group description="Nome:">
            <p class="form-control-static">
                {{$data->name}}
            </p>
        </x-show.form-group>
        <x-show.form-group description="Email:">
            <p class="form-control-static">
                {{$data->email}}
            </p>
        </x-show.form-group>
        <x-show.form-group description="Regras:">
            @forelse($data->getRoleNames() as $v)
                <span class="badge badge-success mr-1">{{ $v }}</span>
            @empty
            @endforelse
        </x-show.form-group>

        <x-buttons.form-action
            routeBack="{{ route($page['route'].'index') }}"
            routeAction="{{ route($page['route'].'edit',$data->id) }}"
            descriptionAction="Editar"
            iconAction="fa fa-edit"
        />

    </x-admin.internal-card>

</x-admin>


