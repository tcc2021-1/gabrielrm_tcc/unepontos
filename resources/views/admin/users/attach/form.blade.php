{{ csrf_field()}}

<p class="form-control-static">
   Nome: {{ $data['user']['name'] ?? ' - '  }}
</p>

<p class="form-control-static">
   E-mail: {{ $data['user']['email'] ?? ' - '  }}
</p>

<x-form.select
    description="Empresa"
    name="company_id"
    :values="$data['companies']"
    errorMessage="{{ $errors->first('company_id') }}"
/>
<x-form.select
    description="Instituição"
    name="institution_id"
    :values="$data['institutions']"
    errorMessage="{{ $errors->first('institution_id') }}"
/>

<x-buttons.form-action
    routeBack="{{ route('users.index') }}"
    descriptionAction="Salvar"
    iconAction="fa fa-check"
    submit="true"
/>
