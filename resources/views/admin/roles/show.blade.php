<x-admin title="{{ $page['titlePage'] }}" :breadcrumbs="$page['breadcrumb']">
    <x-admin.internal-card>
        <x-show.form-group description="Nome:">
            <p class="form-control-static">
                {{$data->name}}
            </p>
        </x-show.form-group>
        <x-show.form-group description="Permissões:">
            @if(!empty($data['rolePermission']))
                @foreach($data['rolePermission'] as $v)
                    <label class="badge badge-success p-1 mr-1">{{ $v->name }}</label> <br>
                @endforeach
            @endif
        </x-show.form-group>

        <x-buttons.form-action
            routeBack="{{ route($page['route'].'index') }}"
            routeAction="{{ route($page['route'].'edit',$data->id) }}"
            descriptionAction="Editar"
            iconAction="fa fa-edit"
        />

    </x-admin.internal-card>

</x-admin>
