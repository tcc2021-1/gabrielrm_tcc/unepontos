{{ csrf_field()}}
<x-form.input
    type="text"
    description="Nome"
    name="name"
    value="{{ $data['name'] ?? null  }}"
    errorMessage="{{ $errors->first('name') }}"
/>
<div class="form-group row @if ($errors->has('permission')) has-danger @endif">
    {!! Form::label('permissions', 'Permissões', ['class' => 'col-form-label']) !!}
    <div class="form-group">
            @foreach($data['permission'] as $value)
                <div class="custom-control custom-checkbox">
                    <input class="custom-control-input" @isset($data['rolePermissions']) @if(in_array($value->id, $data['rolePermissions'])) checked @endif @endisset  type="checkbox" id="checkbox-{{$value->id}}" name="permission[]" value="{{$value->id}}">
                    <label for="checkbox-{{$value->id}}" class="custom-control-label">{{$value->name}}</label>
                </div>
            @endforeach
    </div>
    @if ($errors->has('permission'))
        <div class="invalid-feedback">{{ $errors->first('permission') }}</div>
    @endif
</div>
<x-buttons.form-action
    routeBack="{{ route($page['route'].'index') }}"
    descriptionAction="Salvar"
    iconAction="fa fa-check"
    submit="true"
/>


