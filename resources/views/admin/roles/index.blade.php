<x-admin title="{{ $page['titlePage'] }}"
         :breadcrumbs="$page['breadcrumb']"
         createButtonRoute="{{route( $page['route'].'create')}}" >
    <x-admin.internal-card>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 pl-0 pr-0">
                    <table id="d-table" class="d-table table table-striped table-hover text-right display nowrap" width="100%" cellspacing="0">
                        <thead class="thead-dark text-left">
                        <tr>
                        <tr>
                            <th>Nome</th>
                            <th class="actions text-left" width="150px">Ações</th>
                        </tr>
                        </tr>
                        </thead>
                        <tbody align="left">
                        @if(isset($data))
                            @foreach ($data as $key => $obj)
                                <tr>
                                    <td>{{ $obj->name }}</td>
                                    <td class="actions text-left">
                                        <a href="{{ route($page['route'].'show',$obj->id) }}"  class="btn btn-success btn-xs"
                                           data-toggle="tooltip" data-placement="top" title="Visualizar" data-original-title="Visualizar">
                                            <i class="fas fa-eye"></i>
                                        </a>
                                        <a href="{{ route($page['route'].'edit' , $obj->id) }}" class="btn btn-warning btn-xs"
                                           data-toggle="tooltip" data-placement="top" data-original-title="Editar" title="Editar">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                        <a class="btn btn-danger btn-xs" title="Excluir" href="#" data-toggle="modal"
                                           data-target="#delete-modal-{{$obj->id}}">
                                            <i class="fas fa-trash-alt"></i>
                                        </a>
                                        <x-modal.delete
                                            id="{{ $obj->id }}"
                                            title="{{ $page['titlePage'] }}"
                                            routeDelete="{{ route($page['route'].'destroy', $obj->id) }}"
                                        />
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </x-admin.internal-card>
    @push('style')
        <link rel="stylesheet" href="{{ mix('css/datatables.css') }}">
    @endpush
    @push('scripts')
        <script src="{{ mix('js/datatables.js') }}"></script>
        @toastr_render
    @endpush
</x-admin>
