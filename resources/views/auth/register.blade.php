<x-auth.app>
    <h4 class="text-muted font-18 m-b-5 text-center"></h4>
    <p class="text-muted text-center">Preencha os dados para criar uma conta</p>

    <form class="form-horizontal m-t-30" method="POST" action="{{ route('register') }}">
        @csrf

        <div class="form-group row @if ($errors->has('name')) has-danger @endif">
            <div class="col-sm-12">

                {!! Form::label('name','Nome', ['class' => 'col-form-label']) !!}
                {!! Form::text('name', null, array('placeholder' => 'Nome','class' =>$errors->has('name')? 'form-control is-invalid': 'form-control', 'value' => old('name'), 'required' => true)) !!}
                @if ($errors->has('name'))
                    <div class="invalid-feedback">{{ $errors->first('name') }}</div>
                @endif
            </div>
        </div>

        <div class="form-group row @if ($errors->has('email')) has-danger @endif">
            <div class="col-sm-12">

                {!! Form::label('email', __('E-mail'), ['class' => 'col-form-label']) !!}
                {!! Form::text('email', null, array('placeholder' =>  __('E-mail'),'class' =>$errors->has('email')? 'form-control is-invalid': 'form-control', 'value' => old('email'), 'required' => true)) !!}
                @if ($errors->has('email'))
                    <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                @endif
            </div>
        </div>

        <div class="form-group row @if ($errors->has('password')) has-danger @endif">
            <div class="col-sm-12">

                {!! Form::label('password', 'Senha', ['class' => 'col-form-label']) !!}
                {!! Form::password('password', array('class' =>$errors->has('password')? 'form-control is-invalid': 'form-control', 'required' => true, 'type' => 'password')) !!}
                @if ($errors->has('password'))
                    <div class="invalid-feedback">{{ $errors->first('password') }}</div>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-12">
                <label for="password-confirm">Confirme sua senha</label>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-12">
                <button type="submit" class="btn btn-primary">
                     Registrar
                </button>
            </div>
        </div>

    </form>
</x-auth.app>
