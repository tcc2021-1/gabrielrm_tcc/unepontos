<x-auth.app>
    <h4 class="text-muted font-18 m-b-5 text-center"></h4>
    <p class="text-muted text-center">Entre para iniciar uma nova sessão</p>

    <form class="form-horizontal m-t-30" method="POST" action="{{ route('login') }}">
        @csrf

        <div class="form-group row @if ($errors->has('email')) has-danger @endif">
            <div class="col-sm-12">
                {!! Form::label('email', 'Email', ['class' => 'col-form-label']) !!}
                {!! Form::text('email', null, array('placeholder' => 'Email','class' =>$errors->has('email')? 'form-control is-invalid': 'form-control')) !!}
                @if ($errors->has('email'))
                    <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                @endif
            </div>
        </div>

        <div class="form-group row @if ($errors->has('password')) has-danger @endif">
            <div class="col-sm-12">
                {!! Form::label('password', 'Senha', ['class' => 'col-form-label']) !!}
                {!! Form::password('password', array('placeholder' => 'Senha','class' =>$errors->has('password')? 'form-control is-invalid': 'form-control')) !!}
                @if ($errors->has('password'))
                    <div class="invalid-feedback">{{ $errors->first('password') }}</div>
                @endif
            </div>

        </div>

        <div class="form-group row m-t-20">
            <div class="col-6">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customControlInline">
                    <label class="custom-control-label" for="customControlInline">Lembrar me</label>
                </div>
            </div>
            <div class="col-6 text-right">
                <button class="btn btn-primary w-md waves-effect waves-light" type="submit">Entrar
                </button>
            </div>
        </div>
{{--        <div class="form-group m-t-10 mb-0 row">--}}
{{--            <div class="col-12 m-t-20">--}}
{{--                @if (Route::has('password.request'))--}}
{{--                    <a href="{{ route('password.request') }}" class="text-muted">--}}
{{--                        <i class="fas fa-lock"></i> Esqueci minha senha--}}
{{--                    </a>--}}
{{--                @endif--}}
{{--            </div>--}}
{{--        </div>--}}
        <div class="form-group m-t-10 mb-0 row">
            <div class="col-12 m-t-20">
                @if (Route::has('register'))
                    <a href="{{ route('register') }}" class="text-muted">
                        <i class="fas fa-user-plus"></i> Crie sua conta
                    </a>
                @endif
            </div>
        </div>
    </form>
</x-auth.app>
