@extends('layouts.auth.app')
@section('content')
    <div class="row">
        <div class="col-12 offset-md-3 col-md-6 offset-lg-4 col-lg-4">
            <div class="card">
                <div class="card-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-10 offset-1">
                                <img src="{{asset('/images/logo-large.png')}}" class="img-fluid" alt="Logo {{ env('APP_NAME') }}">
                            </div>
                        </div>
                    </div>

                    <div class="p-1">
                        <h4 class="text-muted font-18 m-b-5 text-center"></h4>
                        <p class="text-muted text-center">Redefinir Senha</p>

                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form method="POST" action="{{ route('password.email') }}">
                            @csrf

                            <div class="form-group row @if ($errors->has('email')) has-danger @endif">
                                <div class="col-sm-12">
                                    {!! Form::label('email', 'Email', ['class' => 'col-form-label']) !!}
                                    {!! Form::text('email', null, array('placeholder' => 'Email','autofocus','class' =>$errors->has('email')? 'form-control is-invalid': 'form-control')) !!}
                                    @if ($errors->has('email'))
                                        <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-12 col-md-6 mb-3 mb-md-0">
                                    <a href="{{ route('login')}}" class="btn btn-light btn-block">
                                        <i class="fas fa-long-arrow-alt-left"></i> Voltar</a>
                                </div>
                                <div class="col-12 col-md-6">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        Redefinição de senha
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

