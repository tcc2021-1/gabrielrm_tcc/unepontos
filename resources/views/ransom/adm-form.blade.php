{{ csrf_field()}}
<x-form.select
    description="Alterar Status"
    name="status"
    :values="$data['option_status']"
    valueSelected="{{ $data->getRawOriginal('status') ?? null  }}"
    errorMessage="{{ $errors->first('status') }}"
/>

<x-form.input-file
    description="Comprovante"
    name="file"
    value="{{ $data['file'] ?? null }}"
    errorMessage="{{ $errors->first('file') }}"
/>

<x-buttons.form-action
    routeBack="{{ route($page['route'].'index') }}"
    descriptionAction="Salvar"
    iconAction="fa fa-check"
    submit="true"
/>
