<x-admin title="{{ $page['titlePage'] }}" :breadcrumbs="$page['breadcrumb']">
    <x-admin.internal-card>
        <x-show.form-group description="Solicitado por:">
            <p class="form-control-static">
                {{ $data->user->id }} - {{ $data->user->name }}
            </p>
        </x-show.form-group>

        <x-show.form-group description="Instituição:">
            <p class="form-control-static">
                {{ $data->institution->id }} - {{ $data->institution->name }}
            </p>
        </x-show.form-group>

        <x-show.form-group description="Solicitado em:">
            <p class="form-control-static">
                {{ $data->created_at }}
            </p>
        </x-show.form-group>

        <x-show.form-group description="Pontos solicitados">
            <p class="form-control-static">
                {{ $data->points }}
            </p>
        </x-show.form-group>

        @if(! empty($data->rescuedBy))
            <x-show.form-group description="Resgatado por:">
                <p class="form-control-static">
                    {{ $data->rescued->id }} - {{ $data->rescued->name }}
                </p>
            </x-show.form-group>
        @else
            <x-show.form-group description="Status:">
                <p class="form-control-static">
                    {{ $data->status }}
                </p>
            </x-show.form-group>
        @endif
        <hr />
        <x-form route="{{ route($page['route'].'update', $data->id) }}" method="PUT" enctype="multipart/form-data">
            @include( $page['viewDir'].'adm-form')
        </x-form>
    </x-admin.internal-card>
    @push('scripts')
        @toastr_render
    @endpush
</x-admin>
