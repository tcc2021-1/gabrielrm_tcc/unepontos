<x-admin title="{{ $page['titlePage'] }}" :breadcrumbs="$page['breadcrumb']">
    <x-admin.internal-card>
        <x-show.form-group description="Solicitado por:">
            <p class="form-control-static">
                {{ $data->user->id }} - {{ $data->user->name }}
            </p>
        </x-show.form-group>

        <x-show.form-group description="Instituição:">
            <p class="form-control-static">
                {{ $data->institution->id }} - {{ $data->institution->name }}
            </p>
        </x-show.form-group>

        <x-show.form-group description="Solicitado em:">
            <p class="form-control-static">
                {{ $data->created_at }}
            </p>
        </x-show.form-group>

        <x-show.form-group description="Pontos solicitados">
            <p class="form-control-static">
                {{ $data->points }}
            </p>
        </x-show.form-group>
        @if(! empty($data->rescued_by))
            <x-show.form-group description="Resgatado por:">
                <p class="form-control-static">
                    {{ $data->rescued->name }} - {{ $data->withdrawal_request_date }}
                </p>
            </x-show.form-group>
            <x-show.form-group description="Comprovante:">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalVoucher">
                    Ver comprovante
                </button>
            </x-show.form-group>

            <!-- Modal -->
            <div class="modal fade" id="modalVoucher" tabindex="-1" aria-labelledby="voucherModal" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="voucherModal">Comprovante</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body text-center">
                            <img src="{{ $data->rescueVoucherImage() }}" class="img elevation-2" style="max-width: 1050px" alt="Imagem de comprovante">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <a href="{{ $data->rescueVoucherImage() }}"  class="btn btn-default" title="download" data-original-title="Fazer download">
                                <i class="fas fa-download"></i> Fazer downlaod
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <x-show.form-group description="Status:">
                <p class="form-control-static">
                    {{ $data->status }}
                </p>
            </x-show.form-group>
        @endif

    </x-admin.internal-card>
</x-admin>
