{{ csrf_field()}}
<x-form.select
    description="Instituição"
    name="institution_id"
    :values="$data['institutions']"
    errorMessage="{{ $errors->first('institution_id') }}"
/>
<x-buttons.form-action
    routeBack="{{ route($page['route'].'index') }}"
    descriptionAction="Criar"
    iconAction="fa fa-check"
    submit="true"
/>
