{{ csrf_field()}}
<x-form.select
    description="Instituição"
    name="institution_id"
    :values="$data['institutions']"
    errorMessage="{{ $errors->first('institution_id') }}"
/>
<x-form.input
    type="number"
    description="Pontos"
    name="points"
    value="{{ $data['points'] ?? null  }}"
    errorMessage="{{ $errors->first('points') }}"
/>
<x-buttons.form-action
    routeBack="{{ route($page['route'].'index') }}"
    descriptionAction="Salvar"
    iconAction="fa fa-check"
    submit="true"
/>
