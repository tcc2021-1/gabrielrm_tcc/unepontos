<x-admin title="{{ $page['titlePage'] }}" :breadcrumbs="$page['breadcrumb']">
    <x-admin.internal-card>
        <x-show.form-group description="Criado por:">
            <p class="form-control-static">
                {{ $data->user->id }} - {{ $data->user->name }}
            </p>
        </x-show.form-group>

        <x-show.form-group description="Instituição:">
            <p class="form-control-static">
                {{ $data->institution->id }} - {{ $data->institution->name }}
            </p>
        </x-show.form-group>

        <x-show.form-group description="Criado em:">
            <p class="form-control-static">
                {{ $data->created_at }}
            </p>
        </x-show.form-group>

        <x-show.form-group description="Pontos doados">
            <p class="form-control-static">
                {{ $data->points }}
            </p>
        </x-show.form-group>

        @if(! empty($data->rescuedBy))
            <x-show.form-group description="Resgatado por:">
                <p class="form-control-static">
                    {{ $data->rescued->id }} - {{ $data->rescued->name }}
                </p>
            </x-show.form-group>
        @else
            <x-show.form-group description="Expira em:">
                <p class="form-control-static">
                    {{ $data->expiration_date }}
                </p>
            </x-show.form-group>
        @endif

    </x-admin.internal-card>
</x-admin>
