<x-admin title="{{ $page['titlePage'] }}" :breadcrumbs="$page['breadcrumb']">
    <div class="row">
        <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-info">
                <div class="inner">
                    <h3>{{ $data['widgets']['coupons_created'] }}</h3>

                    <p>Cupons gerados</p>
                </div>
                <div class="icon">
                    <i class="fas fa-ticket-alt"></i>
                </div>
                <a href="{{ route('coupons.index') }}" class="small-box-footer">
                    Ver cupons <i class="fas fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-danger">
                <div class="inner">
                    <h3>{{ $data['widgets']['coupons_rescued'] }}</h3>

                    <p>Cupons resgatados</p>
                </div>
                <div class="icon">
                    <i class="fas fa-handshake"></i>
                </div>
                <a href="{{ route('coupons.index') }}" class="small-box-footer">
                    Ver cupons <i class="fas fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-gradient-green ">
                <div class="inner">
                    <h3>{{ $data['widgets']['donations'] }}</h3>

                    <p>Doações realizadas</p>
                </div>
                <div class="icon">
                    <i class="fas fa-dove"></i>
                </div>
                <a href="{{ route('donations.index') }}" class="small-box-footer">
                    Doações realizadas <i class="fas fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-warning">
                <div class="inner">
                    <h3>{{ $data['widgets']['created_users'] }}</h3>

                    <p>Usuários registrados</p>
                </div>
                <div class="icon">
                    <i class="fas fa-user-plus"></i>
                </div>
                <a href="{{ route('users.index') }}" class="small-box-footer">
                    Ver usuários <i class="fas fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
    </div>
    <x-admin.internal-card>
        <div class="row row-cols-1 row-cols-md-2">
            <div class="col-md-6">
                <div class="card h-100">
                    <div class="card-header">
                        <h3 class="card-title">Últimos cupons criados</h3>
                    </div>
                    <div class="card-body">
                        <ul class="list-group mt-4">
                            @forelse($data['lastCoupons'] as $coupons)
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    {{ $coupons->company->fantasy }}
                                    <span class="badge badge-primary badge-pill">{{ $coupons->points }}</span>
                                </li>
                            @empty
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    Não existem cupons gerados.
                                </li>
                            @endforelse
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card h-100">
                    <div class="card-header">
                        <h3 class="card-title">Últimas doações realizadas</h3>
                    </div>
                    <div class="card-body text-center">
                        <ul class="list-group mt-4">
                            @forelse($data['lastDonations'] as $donation)
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    {{ $donation->user->name }}
                                    <span class="badge badge-primary badge-pill">{{ $donation->points }}</span>
                                </li>
                            @empty
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    Não existem doações realizadas.
                                </li>
                            @endforelse
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-md-6">
                <canvas id="chartDonations" style="width:100%; max-height: 500px;"></canvas>
            </div>
            <div class="col-md-6">
                <canvas id="chartCoupons" style="width:100%; max-height: 500px;"></canvas>
            </div>
        </div>
    </x-admin.internal-card>
    @push('scripts')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.5.1/chart.min.js" integrity="sha512-Wt1bJGtlnMtGP0dqNFH1xlkLBNpEodaiQ8ZN5JLA5wpc1sUlk/O5uuOMNgvzddzkpvZ9GLyYNa8w2s7rqiTk5Q==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        @toastr_render
        <script>
            $(function () {
                var donations =  {!! json_encode($data['donationsByMonth']) !!};
                var donationsXValues = donations.label;
                var donationsYValues = donations.data;
                var donationsSumPoints = donationsYValues.reduce((donationsAux, numberDonationsAux) => {
                    return donationsAux += numberDonationsAux;
                }, 0)
                var coupons =  {!! json_encode($data['couponsByMonth']) !!};
                var cuponsXValues = coupons.label;
                var cuponsYValues = coupons.data;
                var cuponsSumPoints = cuponsYValues.reduce((cuponsAux, numberCouponsAux) => {
                    return cuponsAux += numberCouponsAux;
                }, 0)

                const CHART_COLORS = {
                    green: 'rgb(75, 192, 192)',
                    blue: 'rgb(54, 162, 235)',
                    purple: 'rgb(153, 102, 255)',
                    yellow: 'rgb(255, 205, 86)',
                    red: 'rgb(255, 99, 132)',
                    orange: 'rgb(255, 159, 64)',
                    grey: 'rgb(201, 203, 207)'
                };

                new Chart("chartDonations", {
                    type: "doughnut",
                    data: {
                        labels: cuponsXValues,
                        datasets: [
                            {
                                label: 'Pontos',
                                data: cuponsYValues,
                                backgroundColor: Object.values(CHART_COLORS),
                            }
                        ]
                    },
                    options: {
                        responsive: true,
                        plugins: {
                            legend: {
                                position: 'top',
                            },
                            title: {
                                display: true,
                                text: `Pontos criados nos últimos 6 meses: ${cuponsSumPoints} `
                            }
                        }
                    },
                });
                new Chart("chartCoupons", {
                    type: "doughnut",
                    data: {
                        labels: donationsXValues,
                        datasets: [
                            {
                                label: 'Pontos',
                                data: donationsYValues,
                                backgroundColor: Object.values(CHART_COLORS),
                            }
                        ]
                    },
                    options: {
                        responsive: true,
                        plugins: {
                            legend: {
                                position: 'top',
                            },
                            title: {
                                display: true,
                                text: `Pontos doados nos últimos 6 meses: ${donationsSumPoints} `
                            }
                        }
                    },
                });
            });
        </script>
    @endpush
</x-admin>

