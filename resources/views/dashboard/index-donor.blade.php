<x-admin title="{{ $page['titlePage'] }}" :breadcrumbs="$page['breadcrumb']">
    <x-admin.internal-card>
        <div class="row row-cols-1 row-cols-md-2">
            <div class="col-md-6">
                <div class="card h-100">
                    <div class="card-header">
                        <h3 class="card-title">Novo cupom?</h3>
                    </div>
                    <div class="card-body text-center">
                        <x-form route="{{ route('rescued-coupon.store') }}" method="POST" enctype="multipart/form-data">
                            @include( $page['viewDir'].'donor.form')
                        </x-form>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card h-100">
                    <div class="card-header">
                        <h3 class="card-title">Pontos</h3>
                    </div>
                    <div class="card-body text-center">
                        @if($data['availablePoints'] > 0)
                            <h1>Você possui o total de <span class="badge badge-info">{{ $data['availablePoints'] }}</span> pontos.</h1>
                            <a href="{{ route('donate.create') }}" class="btn btn-info btn-lg"
                               data-toggle="tooltip" data-placement="top" data-original-title="Doar" title="DOAR">
                                <i class="fas fa-hand-holding-heart"></i> Doar
                            </a>
                        @else
                            <h2>Opa, você ainda não possui pontos.</h2>
                            <h5>Efetue compras em nossos parceiros e ganhe cupons.</h5>
                            <a href="{{ route('donate.create') }}" class="btn btn-info btn-lg"
                               data-toggle="tooltip" data-placement="top" data-original-title="Doar" title="DOAR">
                                <i class="fas fas fa-store-alt"></i> Parceiros
                            </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-md-12">
                <h2><span class="bd-content-title">Algumas instituições</span></h2>
            </div>
        </div>
        <div class="row">
            @forelse($data['institutions'] as $institution)
                <div class="col-md-4">
                    <div class="card h-100">
                        <img src="{{ $institution->coverImage }}" class="img elevation-2" style="height: auto; width: auto; max-width:100%; max-height:200px" alt="Capa da instituição">
                        <div class="card-body">
                            <h5 class="card-title">{{ $institution->name }}</h5>

                            <p class="card-text">{!! Str::of($institution->description)->before('<br />')->limit(500, '...') !!}</p>
                        </div>
                        <div class="card-footer clearfix">
                            <a href="{{ route('institutions.show', $institution->id) }}" class="btn btn-info float-right">Mais detalhes</a>
                        </div>
                    </div>
                </div>
            @empty
                <div class="col-md-12 text-center">
                    <p>Sem instituições cadastradas no momento.</p>
                </div>
            @endforelse
        </div>
        @if($data['institutions']->isNotEmpty())
            <div class="row mt-3">
                <div class="col-md-12 text-center">
                    <a href="{{ route('institutions.index') }}">Veja todas instituições cadastradas.</a>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <h2><span class="bd-content-title">Alguns parceiros</span></h2>
            </div>
        </div>
        <div class="row">
            @forelse($data['companies'] as $company)
                <div class="col-md-4">
                    <div class="card h-100">
                        <img src="{{ $company->coverImage }}" class="img elevation-2" style="height: auto; width: auto; max-width:100%; max-height:200px" alt="Capa da empresa">
                        <div class="card-body">
                            <h5 class="card-title">{{ $company->fantasy }}</h5>
                            <p class="card-text">{!! Str::of($company->description)->before('<br />')->limit(500, '...') !!}</p>
                        </div>
                        <div class="card-footer clearfix">
                            <a href="{{ route('companies.show', $company->id) }}" class="btn btn-info float-right">Mais detalhes</a>
                        </div>
                    </div>
                </div>
            @empty
                <div class="col-md-12 text-center">
                    <p>Sem empresas parceiras cadastradas no momento.</p>
                </div>
            @endforelse
        </div>
        @if($data['companies']->isNotEmpty())
            <div class="row mt-3">
                <div class="col-md-12 text-center">
                    <a href="{{ route('companies.index') }}">Veja todos parceiros cadastrados.</a>
                </div>
            </div>
        @endif

    </x-admin.internal-card>
    @push('scripts')
        @toastr_render
    @endpush
</x-admin>

