<x-admin title="{{ $page['titlePage'] }}" :breadcrumbs="$page['breadcrumb']">
    <x-admin.internal-card>
        Conteudo da dashboard.
    </x-admin.internal-card>
    @push('scripts')
        @toastr_render
    @endpush
</x-admin>

