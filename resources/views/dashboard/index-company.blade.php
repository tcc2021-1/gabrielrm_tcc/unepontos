<x-admin title="{{ $page['titlePage'] }}" :breadcrumbs="$page['breadcrumb']">
    <x-admin.internal-card>
        <div class="row row-cols-1 row-cols-md-2">
            <div class="col-md-6">
                <div class="card h-100">
                    <div class="card-body">
                        <a href="{{ route('companies.show', $data['company']->id) }}" class="btn btn-primary btn-lg"
                           data-toggle="tooltip" data-placement="top" data-original-title="Ver Empresa" title="Ver Empresa">
                            <i class="fas fa-eye"></i> Ver {{ $data['company']->fantasy }}
                        </a>
                        <a href="{{ route('companies.edit', $data['company']->id) }}" class="btn btn-info btn-lg"
                           data-toggle="tooltip" data-placement="top" data-original-title="Editar Empresa" title="Editar Empresa">
                            <i class="fa fa-edit"></i> Editar {{ $data['company']->fantasy }}
                        </a>
                        <ul class="list-group mt-4">
                            <li class="list-group-item active d-flex justify-content-between align-items-center">
                                Últimos cupons resgatados
                            </li>
                            @forelse($data['lastCoupons'] as $coupon)
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    {{ $coupon->rescuedBy->name }}
                                    <span class="badge badge-primary badge-pill">{{ $coupon->points }}</span>
                                </li>
                            @empty
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    Essa empresa ainda não teve cupons resgatados.
                                </li>
                            @endforelse
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card h-100">
                    <div class="card-header">
                        <h3 class="card-title">Avisos</h3>
                    </div>
                    <div class="card-body text-center">
                        Sem avisos no momento
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-md-12">
                <canvas id="chartPoints" style="width:100%; max-height: 500px;"></canvas>
            </div>
        </div>
    </x-admin.internal-card>
    @push('scripts')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.5.1/chart.min.js" integrity="sha512-Wt1bJGtlnMtGP0dqNFH1xlkLBNpEodaiQ8ZN5JLA5wpc1sUlk/O5uuOMNgvzddzkpvZ9GLyYNa8w2s7rqiTk5Q==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        @toastr_render
        <script>
            $(function () {
                var coupons =  {!! json_encode($data['couponByMonth']) !!};
                var xValues = coupons.label;
                var yValues = coupons.data;
                var sumPoints = yValues.reduce((acumulador, numero) => {
                    return acumulador += numero;
                }, 0)

                const CHART_COLORS = {
                    green: 'rgb(75, 192, 192)',
                    blue: 'rgb(54, 162, 235)',
                    purple: 'rgb(153, 102, 255)',
                    yellow: 'rgb(255, 205, 86)',
                    red: 'rgb(255, 99, 132)',
                    orange: 'rgb(255, 159, 64)',
                    grey: 'rgb(201, 203, 207)'
                };

                new Chart("chartPoints", {
                    type: "doughnut",
                    data: {
                        labels: xValues,
                        datasets: [
                            {
                                label: 'Pontos',
                                data: yValues,
                                backgroundColor: Object.values(CHART_COLORS),
                            }
                        ]
                    },
                    options: {
                        responsive: true,
                        plugins: {
                            legend: {
                                position: 'top',
                            },
                            title: {
                                display: true,
                                text: `Pontos gerados nos últimos 6 meses: ${sumPoints} `
                            }
                        }
                    },
                });
            });
        </script>
    @endpush
</x-admin>

