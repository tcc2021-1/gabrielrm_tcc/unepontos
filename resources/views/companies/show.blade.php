<x-admin title="{{ $page['titlePage'] }}" :breadcrumbs="$page['breadcrumb']">
    <x-admin.internal-card>
        <div class="row d-flex">
            <div class="col-md-7 mt-3">
                <div class="card h-100">
                    <img src="{{ $data['company']->coverImage }}" class="img elevation-2" style="height: auto; width: auto; max-width:100%; max-height:400px" alt="Capa da empresa">
                    <div class="card-body">
                        <h5 class="card-title">{{ $data['company']->name }}</h5>
                        <p class="card-text">{!! $data['company']->description !!}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-5 mt-3">
                <div class="card">
                    <div class="card-body text-center">
                        @if($data['availablePoints'] > 0)
                            <h1>Você possui o total de <span class="badge badge-info">{{ $data['availablePoints'] }}</span> pontos.</h1>
                            <h5>Veja nossas instituições parceiras.</h5>
                            <div class="card border-transparent">
                                <div class="card-body">
                                    <a href="{{ route('institutions.index') }}" class="btn btn-info btn-lg"
                                       data-toggle="tooltip" data-placement="top" data-original-title="Ver Instituições" title="Ver Instituições">
                                        <i class="fas fas fa-store-alt"></i> Ver Instituições
                                    </a>
                                </div>
                            </div>
                        @else
                            <h2>Opa, você ainda não possui pontos.</h2>
                            <h5>Efetue compras em nossos parceiros e ganhe cupons.</h5>
                            <a href="{{ route('companies.index') }}" class="btn btn-info btn-lg"
                               data-toggle="tooltip" data-placement="top" data-original-title="Ver Parceiros" title="Ver Parceiros">
                                <i class="fas fas fa-store-alt"></i> Ver Parceiros
                            </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </x-admin.internal-card>
    @push('scripts')
        @toastr_render
    @endpush
</x-admin>

