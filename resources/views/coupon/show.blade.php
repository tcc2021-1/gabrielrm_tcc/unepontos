<x-admin title="{{ $page['titlePage'] }}" :breadcrumbs="$page['breadcrumb']">
    <x-admin.internal-card>
        <x-show.form-group description="Criado por:">
            <p class="form-control-static">
                {{ $data->user->id }} - {{ $data->user->name }}
            </p>
        </x-show.form-group>

        <x-show.form-group description="Empresa:">
            <p class="form-control-static">
                {{ $data->company->id }} - {{ $data->company->fantasy }}
            </p>
        </x-show.form-group>

        <x-show.form-group description="Criado em:">
            <p class="form-control-static">
                {{ $data->created_at }}
            </p>
        </x-show.form-group>

        <x-show.form-group description="Pontos Gerados">
            <p class="form-control-static">
                {{ $data->points }}
            </p>
        </x-show.form-group>

        <x-show.form-group description="Código do cupom:">
            <p class="form-control-static">
                {{ $data->code }}
            </p>
        </x-show.form-group>

        @if(! empty($data->rescuedBy))
            <x-show.form-group description="Resgatado por:">
                <p class="form-control-static">
                    {{ $data->rescuedBy->id }} - {{ $data->rescuedBy->name }}
                </p>
            </x-show.form-group>
        @else
            <x-buttons.form-action
                routeBack="{{ route($page['route'].'index') }}"
                routeAction="{{ route($page['route'].'edit',$data->id) }}"
                descriptionAction="Editar"
                iconAction="fa fa-edit"
            />
        @endif

    </x-admin.internal-card>
</x-admin>
