{{ csrf_field()}}
<x-form.select
    description="Empresa"
    name="company_id"
    :values="$data['companies']"
    errorMessage="{{ $errors->first('company_id') }}"
/>
<x-form.input
    type="number"
    description="Pontos"
    name="points"
    value="{{ $data['points'] ?? null  }}"
    errorMessage="{{ $errors->first('points') }}"
/>
<x-buttons.form-action
    routeBack="{{ route($page['route'].'index') }}"
    descriptionAction="Salvar"
    iconAction="fa fa-check"
    submit="true"
/>


