<div class="modal fade" id="delete-modal-{{ $id }}" tabindex="-1" aria-hidden="true"
     benefit="dialog" aria-labelledby="modalLabel">
    <div class="modal-dialog modal-sm" benefit="document">
        <div class="modal-content bg-light">
            <div class="modal-header border-bottom-0">
                <h5 class="modal-title" id="modalLabel">
                    Excluir {{ $title }}</h5>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    Deseja realmente excluir este(a) {{ $title }}?
                </div>
            </div>
            <div class="modal-footer border-top-0">
                <form action="{{ $routeDelete }}" method="POST" style="display:inline">
                    @csrf
                    <input type="hidden" name="_method" value="DELETE">
                    <button class="btn btn-danger">Sim</button>
                </form>
                <input type="hidden" id="dado_modal">
                <button type="button" class="btn btn-warning" data-dismiss="modal">
                    N&atilde;o
                </button>
            </div>
        </div>
    </div>
</div>
