<div class="form-group row @if ($errors->has('role_id')) has-danger @endif">
    <label for="{{ $name }}" class="col-form-label">{{ $description }}</label>
    <button class="btn btn-light form-control @if($errorMessage) is-invalid @endif" data-icon="{{ old($name) ?? $value }}" name="icon" id="icon"></button>
    @if($errorMessage)
        <div class="invalid-feedback">{{ $errorMessage }}</div>
    @endif
</div>
