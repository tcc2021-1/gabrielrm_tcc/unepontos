<div class="form-group row @if ($errors->has($errorMessage)) has-danger @endif">
    <label for="{{ $name }}" class="col-form-label">{{ $description }}</label>
    <select name="{{ $name }}" id="" class="form-control @if($errorMessage) is-invalid @endif">
        <option value="" selected> Selecione</option>
        @foreach($values as $key => $value)
            <option value="{{ $key }}"  @if($key == $valueSelected) selected @endif > {{ $value }}</option>
        @endforeach
    </select>
    @if($errorMessage)
        <div class="invalid-feedback">{{ $errorMessage }}</div>
    @endif
</div>
