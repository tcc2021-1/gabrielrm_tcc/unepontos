<div class="row">
    <div class="col-12 p-0 m-0">
        <label for="{{ $name }}" class="col-form-label">{{ $description }}</label>
    </div>
    <div class="col-12 p-0 m-0">
        <div class="form-group form-check">
            <input type="checkbox" class="form-check-input" value="1" @if($checked) checked @endif name="{{ $name }}" id="{{ $name }}">
            <label class="form-check-label" for="{{ $name }}">Ativo</label>
        </div>
    </div>
</div>
