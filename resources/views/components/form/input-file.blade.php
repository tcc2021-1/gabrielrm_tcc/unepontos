<div class="form-group row @if($errorMessage) has-danger @endif">
    <label for="{{ $name }}" class="col-form-label">{{ $description }}</label>
    <div class="custom-file">
        <input type="file" value="{{ old($name) ?? $value }}" name="{{ $name }}" class="form-control custom-file-input @if($errorMessage) is-invalid @endif">
        <label for="file" class="custom-file-label">Escolha um arquivo</label>
    </div>

    @if($errorMessage)
        <small class="text-danger">{{ $errorMessage }}</small>
    @endif
</div>
