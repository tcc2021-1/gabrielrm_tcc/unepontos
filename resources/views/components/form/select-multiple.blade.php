<div class="form-group row @if ($errors->has('role_id')) has-danger @endif">
    <label for="{{ $name }}" class="col-form-label">{{ $description }}</label>
    <select class="searchable form-control  @if($errorMessage) is-invalid @endif" multiple="multiple" id="multi-select" name="{{$name}}[]">
        @foreach($values as $value)
            <option value='{{$value['id']}}' @isset($value['selected']) @if($value['selected']) selected @endif @endisset>{{$value['name']}}</option>
        @endforeach
    </select>

    @if($errorMessage)
        <div class="invalid-feedback">{{ $errorMessage }}</div>
    @endif
</div>
