<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta content="https://unepontos.com.br" name="author">
    <link rel="shortcut icon" href="{{ asset('images/logo.png') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    @stack('style')
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

    <x-admin.nav-bar/>

    <x-admin.main-side-bar/>

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1> {{ $title }}</h1>
                    </div>
                    <div class="col-sm-6">
                        <x-admin.breadcrumb :breadcrumbs="$breadcrumbs"/>
                    </div>
                    @if($createButtonRoute)
                        <div class="col-12 text-right">
                            <a href="{{$createButtonRoute}}" class="btn btn-primary text-white">
                                <i class="fa fa-plus"></i> Criar
                            </a>
                        </div>
                    @endif
                </div>
            </div>
        </section>

        <section class="content">
            {{ $slot }}
        </section>
    </div>

    <x-admin.footer/>

    <aside class="control-sidebar control-sidebar-dark">
    </aside>
</div>
<script src="{{ asset('js/app.js') }}"></script>
@stack('scripts')
</body>
</html>
