<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
    <title>{{ config('app.name', 'UnePontos') }}</title>
    <meta content="https://unepontos.com.br" name="author">
    <link rel="shortcut icon" href="{{ asset('images/logo.png') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body class="bg-auth">
<div class="wrapper-page pt-5">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 offset-md-3 col-md-6 offset-lg-4 col-lg-4">
                <div class="card">
                    <div class="card-body">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-10 offset-1">
                                    <img src="{{asset('/images/up_logo_completo_cor.svg')}}" class="img-fluid" alt="Logo {{ config('app.name', 'UnePontos') }}">
                                </div>
                            </div>
                        </div>
                        <div class="p-3">
                            {{ $slot }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="m-t-40 text-center">
</div>
<script src="{{ asset('js/app.js') }}"></script>
</body>

</html>
