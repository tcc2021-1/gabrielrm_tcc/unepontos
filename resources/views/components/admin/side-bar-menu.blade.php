<nav class="mt-2" style="overflow: hidden;">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        @foreach($menus as $item)
            @if($item->children->count() > 0)
                <li class="nav-item has-treeview">
                    <a href="javascript:void(0)" class="nav-link">
                        <i class="nav-icon {{$item->icon}}"></i>
                        <p>
                            {{$item->title}}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        @foreach($item->children as $submenu)
                            <li class="nav-item">
                                <a href="{{url(($submenu->url ?? '')  )}}" class="nav-link">
                                    <i class="nav-icon {{$submenu->icon}}"></i>
                                    <p>{{$submenu->title}}</p>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </li>
            @else
                <li class="nav-item has-treeview">
                    <a href="@isset($item->url){{ url($item->url) }} @else# @endisset" class="nav-link">
                        <i class="nav-icon {{$item->icon}}"></i>
                        <p>{{$item->title}}</p>
                    </a>
                </li>
            @endif
        @endforeach
    </ul>
</nav>
