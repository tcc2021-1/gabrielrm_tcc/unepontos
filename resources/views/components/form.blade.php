<form action="{{ $route }}"
      method="{{ $method == 'GET' ? 'GET' : 'POST'}}"
      enctype="{{ $enctype }}"
      class="needs-validation form-horizontal"
      novalidate >

    @if($method == 'PUT')
        <input type="hidden" value="PUT" name="_method">
    @endif
    {{ $slot }}
</form>
