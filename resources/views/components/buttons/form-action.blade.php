<hr>
<div class="form-actions float-left">
    <a href="{{ $routeBack }}" class="btn btn-light">
        <i class="fas fa-long-arrow-alt-left"></i> Voltar
    </a>
</div>
<div class="form-actions float-right">
    @if(isset($submit))
        <button type="submit" name="save" class="btn btn-info">
            <i class="{{ $iconAction }}"></i> {{ $descriptionAction }}
        </button>
    @else
        <a href="{{ $routeAction }}" class="btn btn-warning">
            <i class="{{ $iconAction }}"></i> {{ $descriptionAction }}
        </a>
    @endif
</div>
