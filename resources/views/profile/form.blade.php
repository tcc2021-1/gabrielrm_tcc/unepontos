{{ csrf_field()}}
<x-form.input
    type="text"
    description="Nome"
    name="name"
    value="{{ $data['name'] ?? null  }}"
    errorMessage="{{ $errors->first('name') }}"
/>
<x-form.input
    type="email"
    description="Email"
    name="email"
    value="{{ $data['email'] ?? null }}"
    errorMessage="{{ $errors->first('email') }}"
/>
<x-form.input
    type="password"
    description="Senha"
    name="password"
    value=""
    errorMessage="{{ $errors->first('password') }}"
/>
<x-form.input
    type="password"
    description="Confirmar senha"
    name="confirm-password"
    value=""
    errorMessage="{{ $errors->first('confirm-password') }}"
/>
<x-form.input-file
    description="Foto"
    name="file"
    value="{{ $data['file'] ?? null }}"
    errorMessage="{{ $errors->first('file') }}"
/>

<x-buttons.form-action
    routeBack="{{ route('home') }}"
    descriptionAction="Salvar"
    iconAction="fa fa-check"
    submit="true"
/>
