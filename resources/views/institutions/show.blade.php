<x-admin title="{{ $page['titlePage'] }}" :breadcrumbs="$page['breadcrumb']">
    <x-admin.internal-card>
        <div class="row d-flex">
            <div class="col-md-7 mt-3">
                <div class="card h-100">
                    <img src="{{ $data['institution']->coverImage }}" class="img elevation-2" style="height: auto; width: auto; max-width:100%; max-height:400px" alt="Capa da instituição">
                    <div class="card-body">
                        <h5 class="card-title">{{ $data['institution']->name }}</h5>
                        <p class="card-text">{!! $data['institution']->description !!}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-5 mt-3">
                <div class="card">
                    <div class="card-body text-center">
                        @if($data['availablePoints'] > 0)
                            <h1>Você possui o total de <span class="badge badge-info">{{ $data['availablePoints'] }}</span> pontos.</h1>
                            <div class="card border-transparent">
                                <div class="card-body">
                                    <x-form route="{{ route('donate.store') }}" method="POST" enctype="multipart/form-data">
                                        @include('institutions.donor.form')
                                    </x-form>
                                </div>
                            </div>
                        @else
                            <h2>Opa, você ainda não possui pontos.</h2>
                            <h5>Efetue compras em nossos parceiros e ganhe cupons.</h5>
                            <a href="{{ route('companies.index') }}" class="btn btn-info btn-lg"
                               data-toggle="tooltip" data-placement="top" data-original-title="Ver Parceiros" title="Ver Parceiros">
                                <i class="fas fas fa-store-alt"></i> Ver Parceiros
                            </a>
                        @endif
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <ul class="list-group">
                            <li class="list-group-item active d-flex justify-content-between align-items-center">
                                Últimas doações
                            </li>
                            @forelse($data['donations'] as $donation)
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    {{ $donation->user->name }}
                                    <span class="badge badge-primary badge-pill">{{ $donation->points }}</span>
                                </li>
                            @empty
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    Essa instituição ainda não recebeu uma doação, seja o primeiro!
                                </li>
                            @endforelse
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </x-admin.internal-card>
    @push('scripts')
        @toastr_render
    @endpush
</x-admin>

