{{ csrf_field()}}
<div class="form-group row @if($errors->first('points')) has-danger @endif">
    <input type="hidden" name="institution_id" value="{{ $data['institution']->id }}">
    <label for="code" class="col-form-label">Informe o número de pontos</label>
    <div class="input-group input-group-sm">
        <input type="text" value="{{ old('points') ?? null }}" name="points" class="form-control @if($errors->first('points')) is-invalid @endif">
        <span class="input-group-append">
            <button type="submit" name="save" class="btn btn-info btn-flat"><i class="fas fa-hand-holding-heart"></i> Doar</button>
          </span>
        @if($errors->first('points'))
            <div class="invalid-feedback">{{ $errors->first('points') }}</div>
        @endif
    </div>

</div>
