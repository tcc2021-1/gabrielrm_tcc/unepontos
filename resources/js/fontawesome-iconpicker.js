import './bootstrap';
import $ from "jquery";
import './plugins/fontawesome5-3-1.min';
import './plugins/bootstrap-iconpicker.min';


$('#icon').iconpicker({
    align: 'center', // Only in div tag
    arrowPrevIconClass: 'fas fa-angle-left',
    arrowNextIconClass: 'fas fa-angle-right',
    cols: 8,
    footer: true,
    header: true,
    iconset: 'fontawesome5',
    labelHeader: '{0} de {1} páginas',
    labelFooter: '{0} - {1} de {2} ícones',
    placement: 'bottom', // Only in button tag
    rows: 8,
    search: true,
    searchText: 'Pesquise',
    selectedClass: 'btn-success',
    unselectedClass: ''
});
