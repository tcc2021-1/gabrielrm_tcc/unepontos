<?php

namespace Database\Factories;

use App\Models\Company;
use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Company::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'fantasy' => $this->faker->name(),
            'company_name' => $this->faker->name(),
            'cnpj' => '12345678912345',
            'description' =>  $this->faker->text(250),
        ];
    }
}
