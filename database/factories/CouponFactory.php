<?php

namespace Database\Factories;

use App\Models\Company;
use App\Models\Coupon;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class CouponFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Coupon::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::factory()->create()->id,
            'company_id' => Company::factory()->create()->id,
            'code' => uniqid(),
            'points' => $this->faker->randomNumber(2),
        ];
    }


    public function rescued()
    {
        return $this->state(function (array $attributes) {
            return [
                'rescued' => true,
                'rescued_by' => User::factory()->create()->id,
            ];
        });
    }
}
