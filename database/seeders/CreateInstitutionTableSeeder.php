<?php
namespace Database\Seeders;

use App\Models\Company;
use App\Models\Institution;
use Illuminate\Database\Seeder;

class CreateInstitutionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $numberRegister = 10;

        for ($i = 1; $i <= $numberRegister; $i++) {
            $institution = new Institution();
            $institution->name = "Instituição $i";
            $institution->description = "Descrição da instituição $i";
            $institution->cnpj = rand(11111111111111, 999999999999);
            $institution->save();
        }

        for ($i = 1; $i <= $numberRegister; $i++) {
            $company = new Company();
            $company->fantasy = "Nome fantasia da empresa $i";
            $company->company_name = "Razão social da empresa $i";
            $company->description = "Descrição da empresa $i";
            $company->cnpj = rand(11111111111111, 999999999999);
            $company->save();
        }


    }
}
