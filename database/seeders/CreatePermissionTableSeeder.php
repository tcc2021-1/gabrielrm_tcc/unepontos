<?php
namespace Database\Seeders;

use App\Traits\SeederInsertTrait;
use Illuminate\Database\Seeder;

class CreatePermissionTableSeeder extends Seeder
{
    use SeederInsertTrait;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            [
                'name' =>'company',
                'father' => 'Empresa'
            ],
            [
                'name' =>'institution',
                'father' => 'Instituição'
            ],
            [
                'name' =>'wallet',
                'father' => 'Carteira'
            ],
            [
                'name' =>'admin',
                'father' => 'Admin'
            ],
        ];

        $this->insertPermission($permissions);

    }
}
