<?php
namespace Database\Seeders\Admin;
use App\Traits\SeederInsertTrait;
use Illuminate\Database\Seeder;

class CreateMenuCompanyTableSeeder extends Seeder
{
    use SeederInsertTrait;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menus = [
            [
                'title' => 'Painel de Controle',
                'icons' => 'fas fa-tachometer-alt',
                'url' => '/dashboard',
                'parent_id' => 0,
                'order' => 1,
            ],
            [
                'title' => 'Cupons',
                'icons' => 'fas fa-ticket-alt',
                'url' => '/coupons',
                'parent_id' => 0,
                'order' => 1,
            ]
        ];

        $this->insertMenuAndMenuRole($menus, 3);
    }
}
