<?php
namespace Database\Seeders\Admin;

use App\Models\Company;
use App\Models\Institution;
use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class CreateUsersTableSeeder extends Seeder
{
    const SET_DEFAULT_PASSWORD = 123123;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Admin',
            'email' => 'admin@unepontos.com.br',
            'password' => bcrypt(self::SET_DEFAULT_PASSWORD),
        ]);
        $user->assignRole('admin');

        $userOneCompany = User::create([
            'name' => 'Empresa',
            'email' => 'empresa@unepontos.com.br',
            'password' => bcrypt(self::SET_DEFAULT_PASSWORD),
        ]);
        $userOneCompany->assignRole('partner');

        Company::where('id', 1)->update(['user_id' => $userOneCompany->id]);

        $userTwoCompany = User::create([
            'name' => 'Empresa 2',
            'email' => 'empresa2@unepontos.com.br',
            'password' => bcrypt(self::SET_DEFAULT_PASSWORD),
        ]);
        $userTwoCompany->assignRole('partner');

        Company::where('id', 2)->update(['user_id' => $userTwoCompany->id]);

        $userOneInstitution = User::create([
            'name' => 'Instituição',
            'email' => 'instituicao@unepontos.com.br',
            'password' => bcrypt(self::SET_DEFAULT_PASSWORD),
        ]);
        $userOneInstitution->assignRole('institution');

        Institution::where('id', 1)->update(['user_id' => $userOneInstitution->id]);

        $userTwoInstitution = User::create([
            'name' => 'Instituição 2',
            'email' => 'instituicao2@unepontos.com.br',
            'password' => bcrypt(self::SET_DEFAULT_PASSWORD),
        ]);
        $userTwoInstitution->assignRole('institution');
        
        Institution::where('id', 2)->update(['user_id' => $userTwoInstitution->id]);

        $userOneDonor = User::create([
            'name' => 'Doador',
            'email' => 'doador@unepontos.com.br',
            'password' => bcrypt(self::SET_DEFAULT_PASSWORD),
        ]);
        $userOneDonor->assignRole('donor');

        $userTwoDonor = User::create([
            'name' => 'Doador 2',
            'email' => 'doador2@unepontos.com.br',
            'password' => bcrypt(self::SET_DEFAULT_PASSWORD),
        ]);
        $userTwoDonor->assignRole('donor');


    }
}
