<?php
namespace Database\Seeders\Admin;

use Illuminate\Database\Seeder;
use App\Models\Permission;
use DB;

class CreatePermissionAdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissionsSufix = [
            'list',
            'create',
            'edit',
            'delete',
            'update',
        ];
        $description = [
            'Listar',
            'Criar',
            'Editar',
            'Apagar',
            'Atualizar',

        ];
        $guard = 'webmaster';

        $permissions = [
            [
                'name' =>'user',
                'father' => 'Usuário'
            ],
            [
                'name' =>'role',
                'father' => 'Regra'
            ],
            [
                'name' =>'menu',
                'father' => 'Menu'
            ],
            [
                'name' =>'permission',
                'father' => 'Permissões'
            ],
            [
                'name' =>'dashboard',
                'father' => 'Painel de controle'
            ],
            [
                'name' =>'profile',
                'father' => 'Perfil'
            ],
            [
                'name' =>'coupon',
                'father' => 'Cupom'
            ],
            [
                'name' =>'donation',
                'father' => 'Doação'
            ],
            [
                'name' =>'company',
                'father' => 'Empresa'
            ],
            [
                'name' =>'institution',
                'father' => 'Instituição'
            ],
            [
                'name' =>'wallet',
                'father' => 'Carteira'
            ],
            [
                'name' =>'rescue',
                'father' => 'Resgate'
            ],
            [
                'name' =>'admin',
                'father' => 'Admin'
            ],
        ];

        foreach ($permissions as $permission) {
            foreach ($permissionsSufix as $key => $value) {
                $permissionId = DB::table('permissions')->insertGetId([
                    'name' => $permission['name']."-".$permissionsSufix[$key],
                    'guard_name' => $guard,
                    'description' => $description[$key],
                    'father' => $permission['father']
                ], true);

                //set permission to role webmaster
                DB::table('role_has_permissions')->insert([
                    'permission_id' => $permissionId,
                    'role_id' => '1',
                ]);

                if ($permission['name'] == 'dashboard' && $permissionsSufix[$key] == "list") {
                    DB::table('role_has_permissions')->insert([
                        'permission_id' => $permissionId,
                        'role_id' => '2',
                    ]);

                    DB::table('role_has_permissions')->insert([
                        'permission_id' => $permissionId,
                        'role_id' => '3',
                    ]);

                    DB::table('role_has_permissions')->insert([
                        'permission_id' => $permissionId,
                        'role_id' => '4',
                    ]);
                }

                if ($permission['name'] == 'institution' || $permission['name'] == 'company' && $permissionsSufix[$key] == "list") {
                    DB::table('role_has_permissions')->insert([
                        'permission_id' => $permissionId,
                        'role_id' => '2',
                    ]);

                    DB::table('role_has_permissions')->insert([
                        'permission_id' => $permissionId,
                        'role_id' => '3',
                    ]);

                    DB::table('role_has_permissions')->insert([
                        'permission_id' => $permissionId,
                        'role_id' => '4',
                    ]);
                }

                if ($permission['name'] == 'company') {
                    if ($permissionsSufix[$key] == "edit" || $permissionsSufix[$key] == "update") {
                        DB::table('role_has_permissions')->insert([
                            'permission_id' => $permissionId,
                            'role_id' => '3',
                        ]);
                    }
                }

                if ($permission['name'] == 'profile') {
                    if ($permissionsSufix[$key] == "edit" || $permissionsSufix[$key] == "update") {
                        DB::table('role_has_permissions')->insert([
                            'permission_id' => $permissionId,
                            'role_id' => '2',
                        ]);

                        DB::table('role_has_permissions')->insert([
                            'permission_id' => $permissionId,
                            'role_id' => '3',
                        ]);

                        DB::table('role_has_permissions')->insert([
                            'permission_id' => $permissionId,
                            'role_id' => '4',
                        ]);
                    }
                }

                if ($permission['name'] == 'coupon') {
                    DB::table('role_has_permissions')->insert([
                        'permission_id' => $permissionId,
                        'role_id' => '3',
                    ]);
                }

                if ($permission['name'] == 'donation') {
                    if ($permissionsSufix[$key] == "list") {
                        DB::table('role_has_permissions')->insert([
                            'permission_id' => $permissionId,
                            'role_id' => '2',
                        ]);
                    }
                }

                if ($permission['name'] == 'rescue') {
                    if ($permissionsSufix[$key] == "list" || $permissionsSufix[$key] == "create") {
                        DB::table('role_has_permissions')->insert([
                            'permission_id' => $permissionId,
                            'role_id' => '2',
                        ]);
                    }
                }
            }

        }
    }
}
