<?php
namespace Database\Seeders\Admin;
use App\Traits\SeederInsertTrait;
use Illuminate\Database\Seeder;

class CreateMenuInstitutionTableSeeder extends Seeder
{
    use SeederInsertTrait;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menus = [
            [
                'title' => 'Painel de Controle',
                'icons' => 'fas fa-tachometer-alt',
                'url' => '/dashboard',
                'parent_id' => 0,
                'order' => 1,
            ],
            [
                'title' => 'Doações',
                'icons' => 'fas fa-handshake',
                'url' => '/donations',
                'parent_id' => 0,
                'order' => 2,
            ],
            [
                'title' => 'Resgates',
                'icons' => 'fas fa-credit-card',
                'url' => '/rescues',
                'parent_id' => 0,
                'order' => 3,
            ],
        ];

        $this->insertMenuAndMenuRole($menus, 2);
    }
}
