<?php
namespace Database\Seeders\Admin;

use Illuminate\Database\Seeder;
use DB;

class CreateRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'name' => 'admin',
                'guard_name' => 'webmaster'
            ],
            [
                'name' => 'institution',
                'guard_name' => 'webmaster'
            ],
            [
                'name' => 'partner',
                'guard_name' => 'webmaster'
            ],
            [
                'name' => 'donor',
                'guard_name' => 'webmaster'
            ],
        ]);
    }
}
