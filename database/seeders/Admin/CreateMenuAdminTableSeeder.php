<?php
namespace Database\Seeders\Admin;
use App\Traits\SeederInsertTrait;
use Illuminate\Database\Seeder;

class CreateMenuAdminTableSeeder extends Seeder
{
    use SeederInsertTrait;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menus = [
            [
                'title' => 'Painel de Controle',
                'icons' => 'fas fa-tachometer-alt',
                'url' => '/dashboard',
                'parent_id' => 0,
                'order' => 1,
            ],
            [
                'title' => 'Admin',
                'icons' => 'fa fa-fw fa-users',
                'url' => null,
                'parent_id' => 0,
                'order' => 2,
                'childrens' => [
                    [
                        'title' => 'Usuários',
                        'icons' => 'fas fa-users-cog',
                        'url' => '/users',
                        'order' => 1,
                    ],
                    [
                        'title' => 'Instituições',
                        'icons' => 'fas fa-dove',
                        'url' => '/institutions',
                        'order' => 2,
                    ],
                    [
                        'title' => 'Empresas',
                        'icons' => 'fas fa-store-alt',
                        'url' => '/companies',
                        'order' => 3,
                    ],
                    [
                        'title' => 'Cupons',
                        'icons' => 'fas fa-ticket-alt',
                        'url' => '/coupons',
                        'parent_id' => 0,
                        'order' => 4,
                    ],
                    [
                        'title' => 'Doações',
                        'icons' => 'fas fa-handshake',
                        'url' => '/donations',
                        'parent_id' => 0,
                        'order' => 5,
                    ],
                    [
                        'title' => 'Resgates',
                        'icons' => 'fas fa-credit-card',
                        'url' => '/rescues',
                        'parent_id' => 0,
                        'order' => 6,
                    ],
                    [
                        'title' => 'Regras',
                        'icons' => 'fas fa-unlock-alt',
                        'url' => '/roles',
                        'order' => 7,
                    ],
                    [
                        'title' => 'Menu',
                        'icons' => 'fas fa-bars',
                        'url' => '/menus',
                        'order' => 8,
                    ],
                    [
                        'title' => 'Permissões',
                        'icons' => 'fas fa-dollar-sign',
                        'url' => '/permissions',
                        'order' => 9,
                    ]

                ]
            ],

        ];
        $this->insertMenuAndMenuRole($menus);
    }
}
