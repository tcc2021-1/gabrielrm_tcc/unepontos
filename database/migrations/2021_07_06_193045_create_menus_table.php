<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('menus', function (Blueprint $table) {
             $table->increments('id');
             $table->string('title', 150)->nullable();
             $table->string('icon', 50)->nullable();
             $table->string('slug', 150)->unique()->nullable();
             $table->string('url')->nullable();
             $table->unsignedInteger('parent_id')->default(0);
             $table->smallInteger('order')->default(0);
             $table->boolean('enabled')->default(1);
             $table->timestamps();
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
