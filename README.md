
# UnePontos

## Introdução e requisitos
O ambiente de desenvolvimento é baseado na tecnologia docker, para montar e rodar o sistema de maneira funcional na sua máquina será necessário os seguintes requisitos:

**Sistemas operacionais:**
Windows 10, Linux(preferencialmente Ubuntu)

**Caso você utilize Windows 10:**
Deve ser instalado e configurado o WSL2(Windows Subsystem for Linux) - [Tutorial](https://docs.microsoft.com/en-us/windows/wsl/install-win10)
Baixar uma distribuição linux da loja da Microsoft (preferencialmente Ubuntu em uma versão LTS)
Deve ser instalado o docker para desktop, assim como configurado a integração - [Download](https://www.docker.com/get-started)

**Caso você utilize Linux**
Instalar e configurar o docker - [Documentação](https://docs.docker.com/engine/install/ubuntu/)

## Baixando e construindo o ambiente de desenvolvimento

A versão estável mais atualizada sempre estará na branch `Master`

#### 1. Baixando o projeto
Para obter o projeto basta clonar o repositório para a sua maquina utilizando(se for windows, clonar através do terminal do sistema operacional baixado na Microsoft Store):
SSH: `git clone git@gitlab.com:tcc2021-1/gabrielrm_tcc/unepontos.git`
HTTP: `git clone https://gitlab.com/tcc2021-1/gabrielrm_tcc/unepontos.git`

#### 2. Levantando o ambiente de desenvolvimento
1. Acesse a pasta `unepontos`, rode o comando `docker-compose up` (se você preferir passe o parâmetro `-d` ao final para não deixar o terminal bloqueado na instalação)
2. O projeto irá baixar e montar todas as imagens necessárias para executar o projeto
3. Após o projeto ter inicializado todos os containers a página inicial do projeto pode ser acessada através da url `127.0.0.1:8096` (ou  `localhost:8096`)

#### 3. Comandos auxiliares
Para a utilização do docker podem ser interessantes alguns comandos como:
`docker ps` - lista todos os containers que estejam em pé ou em processo de subida
`docker ps -a` - lista todos os containers, inclusive os que não estiverem em pé
`docker logs <nome do container>` - Acessa os logs do container desejado
`docker inspect <nome do container>` - Exibe dados de configuração do container como IPs, por exemplo.

#### 4. Acessos

**Local:** http://localhost:8096
**Produção:** http://www.unepontos.com.br/

Acessos ao sistema:

Doadores:
Login: doador@unepontos.com.br
Senha: 123123

Login: doador2@unepontos.com.br
Senha: 123123

Empresas:
Login: empresa@unepontos.com.br
Senha: 123123

Login: empresa2@unepontos.com.br
Senha: 123123

Instituições:
Login: instituicao@unepontos.com.br
Senha: 123123

Login: instituicao2@unepontos.com.br
Senha: 123123

Admin:
Login: admin@unepontos.com.br
Senha: 123123
