<?php

use App\Http\Controllers\Admin\CompanyController;
use App\Http\Controllers\Admin\InstitutionController;
use App\Http\Controllers\Admin\User\UserAttachOrganizationFormController;
use App\Http\Controllers\Admin\User\UserAttachOrganizationUpdateController;
use App\Http\Controllers\CouponController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Admin\MenuController;
use App\Http\Controllers\Admin\PermissionController;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\CityController;
use App\Http\Controllers\Donate\DonateFormController;
use App\Http\Controllers\Donate\DonateStoreController;
use App\Http\Controllers\DonationController;
use App\Http\Controllers\RansomController;
use App\Http\Controllers\RescuedCoupon\RescuedCouponFormController;
use App\Http\Controllers\RescuedCoupon\RescuedCouponStoreController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\RoleController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::group(['middleware' => 'auth',], function () {

    Route::redirect('/', '/dashboard');
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('home');

    Route::resource('roles', RoleController::class, ['parameters' => ['roles' => 'role']]);
    Route::resource('users', UserController::class, ['parameters' => ['users' => 'user']]);
    Route::resource('menus', MenuController::class, ['parameters' => ['menus' => 'menu']]);
    Route::resource('permissions', PermissionController::class, ['parameters' => ['permissions' => 'permission']]);
    Route::resource('institutions', InstitutionController::class, ['parameters' => ['permissions' => 'institution']]);
    Route::resource('companies', CompanyController::class, ['parameters' => ['permissions' => 'company']]);
    Route::resource('coupons', CouponController::class, ['parameters' => ['permissions' => 'coupon']]);
    Route::resource('donations',DonationController::class, ['parameters' => ['permissions' => 'donations']])->except(['edit', 'update', 'destroy']);
    Route::resource('rescues', RansomController::class, ['parameters' => ['permissions' => 'rescue']])->except(['destroy']);

    Route::get('profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::put('profile', [ProfileController::class, 'update'])->name('profile.update');

    Route::get('user/{user}/attach', UserAttachOrganizationFormController::class)->name('user.attach.edit');
    Route::put('user/{user}/attach', UserAttachOrganizationUpdateController::class)->name('user.attach.update');

    Route::get('rescued-coupon', RescuedCouponFormController::class)->name('rescued-coupon.create');
    Route::post('rescued-coupon', RescuedCouponStoreController::class)->name('rescued-coupon.store');

    Route::get('donate', DonateFormController::class)->name('donate.create');
    Route::post('donate', DonateStoreController::class)->name('donate.store');

});

